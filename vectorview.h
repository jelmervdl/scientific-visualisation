#ifndef VECTORVIEW_H
#define VECTORVIEW_H

#include <QObject>
#include <QPoint>
#include <QSize>
#include <QVector3D>

class VectorView: public QObject
{
    Q_OBJECT

public:
    VectorView(QObject *parent = 0);
    virtual QSize size() const = 0;
    virtual size_t depth() const = 0;
    virtual QVector3D valueAt(int x, int y, int z = 0) const = 0;
    virtual QPoint cellAt(int x, int y) const = 0;

signals:
    void changed();
};

#endif // VECTORVIEW_H
