#ifndef SIMULATION_H
#define SIMULATION_H

#include <QObject>
#include <QPoint>
#include <QSharedPointer>
#include <rfftw.h>
#include <assert.h>
#include "scalarview.h"
#include "vectorview.h"

class SimulationScalarView;
class SimulationVectorView;

class Simulation: public QObject
{
    Q_OBJECT
    friend class SimulationScalarView;
    friend class SimulationVectorView;

public:
    typedef float (Simulation::*ScalarAccessorFunctionPtr)(size_t i, size_t j) const;
    typedef QVector3D (Simulation::*VectorAccessorFunctionPtr)(size_t i, size_t j) const;
    typedef ScalarView * (Simulation::*ScalarViewFunctionPtr)() const;
    typedef VectorView * (Simulation::*VectorViewFunctionPtr)() const;

private:
	// velocity field at the current moment
	fftw_real *d_vx;
	fftw_real *d_vy;

	// velocity field at the previous moment
	fftw_real *d_vx0;
	fftw_real *d_vy0;

	// user-controlled simulation forces, steered with the mouse
	fftw_real *d_fx;
	fftw_real *d_fy;

	// smoke density at the current (rho) and previous (rho0) moment
	fftw_real *d_rho;
	fftw_real *d_rho0;

	// simulation domain discretization
	rfftwnd_plan d_plan_rc, d_plan_cr;

    // simulation time step
    double d_dt;

    // fluid viscosity
    float d_visc;

public:
    // Size
    size_t d_n;

public:
	Simulation(size_t n, double dt = 0.4, float visc = 0.001);
	~Simulation();

    QSharedPointer<ScalarView> rho() const;
    QSharedPointer<VectorView> v() const;
    QSharedPointer<VectorView> f() const;

signals:
    void stepped();

public slots:
    void step();
    void setViscosity(float visc);
    void setViscosityExp(int power);
    void setTimeStep(double dt);
    void setTimeStepInMilliseconds(int miliseconds);
    void addForce(int x, int y, double dx, double dy);

protected:
    void timerEvent(QTimerEvent *event) Q_DECL_OVERRIDE;
    float rho(size_t i, size_t j) const;
    QVector3D v(size_t i, size_t j) const;
    QVector3D f(size_t i, size_t j) const;

private:
	void set_forces();
	void solve();
	void diffuse_matter();
	void FFT(int direction, void *vx) const;
	inline size_t IDX(size_t x, size_t y) const
	{
        assert(x + d_n * y >= 0);
        assert(x + d_n * y < d_n * d_n);
		return x + d_n * y;
	}

};

class SimulationScalarView : public ScalarView
{
    Q_OBJECT
public:
    SimulationScalarView(Simulation const *simulation, Simulation::ScalarAccessorFunctionPtr fun);
    virtual float valueAt(int x, int y, int z = 0) const;
    virtual QPoint cellAt(int x, int y) const;
    virtual QSize size() const;
    virtual size_t depth() const;
private:
    Simulation const *d_sim;
    Simulation::ScalarAccessorFunctionPtr const d_fun;
};

class SimulationVectorView: public VectorView
{
    Q_OBJECT
public:
    SimulationVectorView(Simulation const *simulation, Simulation::VectorAccessorFunctionPtr fun);
    virtual QVector3D valueAt(int x, int y, int z = 0) const;
    virtual QPoint cellAt(int x, int y) const;
    virtual QSize size() const;
    virtual size_t depth() const;
private:
    Simulation const *d_sim;
    Simulation::VectorAccessorFunctionPtr const d_fun;
};


#endif
