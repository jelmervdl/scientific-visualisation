#include "linearcolormap.h"

#include <QTextStream>
#include <QDebug>

LinearColorMap* LinearColorMap::fromFile(QFile &file)
{
    if(!file.open(QIODevice::ReadOnly)) {
        qDebug() << "Cannot open colormap:"  << file.errorString();
        return 0;
    }

    QVector<QColor> stops;

    QTextStream in(&file);

    for (int linenr = 1; !in.atEnd(); ++linenr) {
        QString line = in.readLine();
        QStringList fields = line.split(",");

        if (fields.size() != 3) {
            qDebug() << "Skipping line" << linenr;
            continue;
        }

        stops.append(QColor(fields[0].toDouble() * 255,
                fields[1].toDouble() * 255,
                fields[2].toDouble() * 255));
    }

    file.close();
    return new LinearColorMap(stops);
}

LinearColorMap::LinearColorMap()
    : ColorMap(),
      m_stops()
{
    //
}

LinearColorMap::LinearColorMap(QVector<QColor> const &stops)
    : ColorMap(),
      m_stops(stops)
{

}

QVector<QColor> &LinearColorMap::stops()
{
    return m_stops;
}

QColor LinearColorMap::colorAt(float value) const
{
    value = normalize(value);

    float position = value * (m_stops.size() - 1);
    int index = (int) position;
    float remainder = position - index;

    if (index == m_stops.size() - 1)
        return m_stops.at(index);

    QColor left(m_stops.at(index));
    QColor right(m_stops.at(index + 1));

    float r = remainder * left.red()   + (1.0f - remainder) * right.red();
    float g = remainder * left.green() + (1.0f - remainder) * right.green();
    float b = remainder * left.blue()  + (1.0f - remainder) * right.blue();

    return QColor(r, g, b);
}
