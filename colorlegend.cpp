#include "colorlegend.h"

#include <QPaintEvent>
#include <QPainter>
#include <QTooltip>
#include <QDebug>

ColorLegend::ColorLegend(Qt::Orientation orientation, QWidget *parent) : QWidget(parent),
    m_colorMap(0),
    m_orientation(orientation),
    m_scale(1.0),
    m_barWidth(20),
    m_stops(),
    m_selectedStopIndex(-1)
{
    switch (m_orientation) {
    case Qt::Vertical:
        setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Expanding));
        break;
    case Qt::Horizontal:
        setSizePolicy(QSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed));
        break;
    }
}

void ColorLegend::setColorMap(QSharedPointer<ColorMap const> const &colorMap)
{
    if (m_colorMap)
        disconnect(m_colorMap.data(), SIGNAL(limitsChanged(float,float)), this, SLOT(update()));

    m_colorMap = colorMap;
    connect(m_colorMap.data(), SIGNAL(limitsChanged(float,float)), this, SLOT(update()));
    update();
}

void ColorLegend::setScale(float scale)
{
    m_scale = scale;
    update();
}

QSize ColorLegend::minimumSizeHint() const
{
    return QSize(20, 20);
}

QSize ColorLegend::sizeHint() const
{
    switch (m_orientation) {
    case Qt::Horizontal:
        return QSize(100, 20);
        break;
    case Qt::Vertical:
        return QSize(m_barWidth + 30, 100);
    }
}

float ColorLegend::valueAt(float pos) const
{
    //return m_colorMap->minValue() + pos * m_colorMap->maxValue();
    return pos / m_scale;
}

QVector<float> const &ColorLegend::stops() const
{
    return m_stops;
}

void ColorLegend::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);
    switch (m_orientation) {
    case Qt::Horizontal:
        for (int x = event->rect().left(); x < width() && x <= event->rect().right(); ++x) {
            float value = 1.f - (float) x / width();
            QColor color(m_colorMap->colorAt(value));
            painter.fillRect(x, 0, 1, height(), color);
        }

        //painter.drawText(0, 12, width(), height() - 12, Qt::AlignLeft, QString::number(m_colorMap->minValue(), 'f', 2));
        //painter.drawText(0, 12, width(), height() - 12, Qt::AlignRight, QString::number(m_colorMap->maxValue(), 'f', 2));

        break;
    case Qt::Vertical:
        for (int y = event->rect().top(); y < height() && y < event->rect().bottom(); ++y) {
            float value = 1.f - (float) y / height();
            QColor color(m_colorMap->colorAt(value));
            painter.fillRect(1, y, m_barWidth - 2, 1, color);
        }

        for (int i = 0; i < m_stops.size(); ++i) {
            float y = (1.f - m_stops[i]) * height();
            QColor backgroundColor(m_colorMap->colorAt(m_stops[i]));
            painter.setPen(Qt::black);
            painter.drawRect(0, y - 3, m_barWidth - 1, 6);

            // Change foreground color based on contrast. Formula from http://stackoverflow.com/a/3943023
            if ((backgroundColor.red() * 0.299 + backgroundColor.green()*0.587 + backgroundColor.blue()*0.114) > 186.0)
                painter.setPen(Qt::black);
            else
                painter.setPen(Qt::white);

            painter.drawLine(0, y, m_barWidth, y);
        }

        painter.setPen(Qt::black);
        painter.drawText(m_barWidth, 0, width() - m_barWidth, height(), Qt::AlignTop, QString::number(valueAt(1.f), 'f', 2));
        painter.drawText(m_barWidth, 0, width() - m_barWidth, height(), Qt::AlignBottom, QString::number(valueAt(0.f), 'f', 2));

        break;
    }
}

void ColorLegend::mousePressEvent(QMouseEvent *event)
{
    m_mousePressPos = event->pos();

    for (int i = 0; i < m_stops.size(); ++i) {
        if (abs((int) ((1.f - m_stops[i]) * height() - event->y())) <= 5) {
            m_selectedStopIndex = i;
            setCursor(Qt::SizeVerCursor);
            update();
            return;
        }
    }

    QWidget::mousePressEvent(event);
}

void ColorLegend::mouseMoveEvent(QMouseEvent *event)
{
    if (m_selectedStopIndex != -1) {
        float value = 1.f - qBound(0.0f, (float) event->y() / height(), 1.0f);
        m_stops[m_selectedStopIndex] = value;

        QPoint pos = mapToGlobal(QPoint(width(), event->y()));
        QToolTip::showText(pos, QString::number(valueAt(value), 'f', 3));

        update();
    } else {
        QWidget::mouseMoveEvent(event);
    }
}

void ColorLegend::mouseReleaseEvent(QMouseEvent *event)
{
    if (m_selectedStopIndex != -1) {
        if (m_mousePressPos == event->pos()) {
            if (event->button() == Qt::RightButton) {
                emit stopRemoved(m_selectedStopIndex);
                m_stops.remove(m_selectedStopIndex);
            }
        } else {
            emit stopChanged(m_selectedStopIndex, m_stops[m_selectedStopIndex]);
        }

        m_selectedStopIndex = -1;
        emit stopsChanged(m_stops);
        setCursor(Qt::ArrowCursor);
        update();
    } else {
        if (m_mousePressPos == event->pos() && event->button() == Qt::LeftButton) {
            float value = 1.f - qBound(0.0f, (float) event->y() / height(), 1.0f);
            m_stops << value;
            emit stopAdded(value);
            emit stopsChanged(m_stops);
            update();
        }
    }

    QWidget::mouseReleaseEvent(event);
}

bool ColorLegend::event(QEvent *event)
{
    if (event->type() == QEvent::ToolTip) {
        QHelpEvent *helpEvent = static_cast<QHelpEvent *>(event);
        float value = 0.0;
        switch (m_orientation) {
        case Qt::Horizontal:
            value = 1.f - ((float) helpEvent->pos().x() / width());
            break;
        case Qt::Vertical:
            value = 1.f - ((float) helpEvent->pos().y() / height());
            break;
        }

        QToolTip::showText(helpEvent->globalPos(), QString::number(valueAt(value), 'f', 3));
        return true;
    }

    return QWidget::event(event);
}
