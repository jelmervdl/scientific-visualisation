#include "boundvectorview.h"

BoundVectorView::BoundVectorView(QSharedPointer<VectorView const> const &parent, float minimum, float maximum)
    : VectorView(),
      m_parent(parent),
      m_minimum(minimum),
      m_maximum(maximum)
{
    Q_ASSERT(minimum < maximum);
    connect(parent.data(), SIGNAL(changed()), this, SIGNAL(changed()));
    connect(this, SIGNAL(boundsChanged()), this, SIGNAL(changed()));
}

void BoundVectorView::setMinimum(float value)
{
    m_minimum = value;
    emit boundsChanged();
}

void BoundVectorView::setMaximum(float value)
{
    m_maximum = value;
    emit boundsChanged();
}

QVector3D BoundVectorView::valueAt(int x, int y, int z) const
{
    QVector3D value(m_parent->valueAt(x, y, z));

    if (value.length() > m_maximum) {
        return value / (value.length() / m_maximum);
    } else if (value.length() < m_minimum) {
        return value / (value.length() / m_minimum);
    } else {
        return value;
    }
}

QPoint BoundVectorView::cellAt(int x, int y) const
{
    return m_parent->cellAt(x, y);
}

QSize BoundVectorView::size() const
{
    return m_parent->size();
}

size_t BoundVectorView::depth() const
{
    return m_parent->depth();
}
