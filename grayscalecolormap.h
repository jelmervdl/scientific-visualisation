#ifndef GRAYSCALECOLORMAP_H
#define GRAYSCALECOLORMAP_H

#include "colormap.h"

class GrayscaleColorMap : public ColorMap
{
public:
    virtual QColor colorAt(float value) const;
};

#endif // GRAYSCALECOLORMAP_H
