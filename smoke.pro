HEADERS = \
    glwidget.h \
    window.h \
    simulation.h \
    colormap.h \
    grayscalecolormap.h \
    rainbowcolormap.h \
    linearcolormap.h \
    colorlegend.h \
    huecolormap.h \
    scalarview.h \
    vectorview.h \
    interpolatingscalarview.h \
    magnitudeview.h \
    floatslider.h \
    divergenceview.h \
    gradientview.h \
    subsamplingvectorview.h \
    boundvectorview.h \
    boundscalarview.h \
    glcamera.h \
    zebracolormap.h \
    scaledscalarview.h \
    normalisedscalarview.h \
    scalarvolumeview.h

SOURCES = \
    glwidget.cpp \
    main.cpp \
    window.cpp \
    simulation.cpp \
    colormap.cpp \
    grayscalecolormap.cpp \
    rainbowcolormap.cpp \
    linearcolormap.cpp \
    colorlegend.cpp \
    huecolormap.cpp \
    scalarview.cpp \
    vectorview.cpp \
    interpolatingscalarview.cpp \
    magnitudeview.cpp \
    floatslider.cpp \
    divergenceview.cpp \
    gradientview.cpp \
    subsamplingvectorview.cpp \
    boundvectorview.cpp \
    boundscalarview.cpp \
    glcamera.cpp \
    zebracolormap.cpp \
    scaledscalarview.cpp \
    normalisedscalarview.cpp \
    scalarvolumeview.cpp

RESOURCES = \
    shaders/shaders.qrc

DEPENDPATH += $$OUT_PWD

QT += widgets

CONFIG += c++11

# install
target.path = $$[QT_INSTALL_EXAMPLES]/opengl/hellogl2
INSTALLS += target

ICON = MeltingPot.icns
QMAKE_INFO_PLIST +=  $$PWD/default.plist
#QMAKE_POST_LINK += sed -i -e "s/@VERSION@/$$VERSION/g" "./$${TARGET}.app/Contents/Info.plist";

APP_COLORMAP_FILES.files = $$files(colormaps/*.colormap)
APP_COLORMAP_FILES.path = Contents/Resources/Colormaps
QMAKE_BUNDLE_DATA += APP_COLORMAP_FILES

unix: LIBS += -L$$PWD/fftw-2.1.5/lib/ -lfftw -lrfftw

INCLUDEPATH += $$PWD/fftw-2.1.5/include
DEPENDPATH += $$PWD/fftw-2.1.5/include

unix: PRE_TARGETDEPS += $$PWD/fftw-2.1.5/lib/libfftw.a
unix: PRE_TARGETDEPS += $$PWD/fftw-2.1.5/lib/librfftw.a

