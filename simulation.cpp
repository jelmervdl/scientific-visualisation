#include "simulation.h"
#include <rfftw.h>
#include <cmath>
#include <qdebug>
#include <QSize>

namespace {

int clamp(float x)
{
	return x >= 0.0
		? (int) x
		: - (int) (1 - x);
}

}

Simulation::Simulation(size_t n, double dt, float visc)
:
    QObject(),

	d_vx(new fftw_real[n * 2 * (n / 2+1)]),
	d_vy(new fftw_real[n * 2 * (n / 2+1)]),
	
	d_vx0(new fftw_real[n * 2 * (n / 2+1)]),
	d_vy0(new fftw_real[n * 2 * (n / 2+1)]),

	d_fx(new fftw_real[n * n]),
	d_fy(new fftw_real[n * n]),

	d_rho(new fftw_real[n * n]),
	d_rho0(new fftw_real[n * n]),

	d_plan_rc(rfftw2d_create_plan(n, n, FFTW_REAL_TO_COMPLEX, FFTW_IN_PLACE)),
	d_plan_cr(rfftw2d_create_plan(n, n, FFTW_COMPLEX_TO_REAL, FFTW_IN_PLACE)),

    d_dt(dt),
    d_visc(visc),
    d_n(n)
{
	// Set the newly allocated reals to 0.
	for (size_t i = 0; i < d_n * d_n; i++) {
		d_vx[i] = 0;
		d_vy[i] = 0;
		d_vx0[i] = 0;
		d_vy0[i] = 0;

		d_fx[i] = 0;
		d_fy[i] = 0;
		
		d_rho[i] = 0;
		d_rho0[i] = 0;
	}
}

Simulation::~Simulation()
{
    delete[] d_vx;
	delete[] d_vy;
	
	delete[] d_vx0;
	delete[] d_vy0;
	
	delete[] d_fx;
	delete[] d_fy;

	delete[] d_rho;
	delete[] d_rho0;

	// FIXME free d_plan_rc and d_plan_cr
}

void Simulation::FFT(int direction, void *vx) const
{
	if (direction == 1)
		rfftwnd_one_real_to_complex(d_plan_rc, reinterpret_cast<fftw_real*>(vx), reinterpret_cast<fftw_complex*>(vx));
	else
		rfftwnd_one_complex_to_real(d_plan_cr, reinterpret_cast<fftw_complex*>(vx), reinterpret_cast<fftw_real*>(vx));
}

void Simulation::solve()
{
	// FIXME can't we just flip these two arrays?
	for (size_t i = 0; i < d_n * d_n; i++)
	{
		d_vx[i] += d_dt * d_vx0[i];
		d_vx0[i] = d_vx[i];

		d_vy[i] += d_dt * d_vy0[i];
		d_vy0[i] = d_vy[i];
	}

	{ // local scope for x, y, i and j.
		fftw_real x, y;
		size_t i, j;
		for (x = 0.5f / d_n, i = 0; i < d_n ; i++, x += 1.0f / d_n)
		{
			for (y = 0.5f / d_n, j = 0; j < d_n; j++, y += 1.0f / d_n)
		    {
				fftw_real x0 = d_n * (x - d_dt * d_vx0[IDX(i,j)])-0.5f;
		        fftw_real y0 = d_n * (y - d_dt * d_vy0[IDX(i,j)])-0.5f;
		      
		        int i0 = clamp(x0);
		        fftw_real s = x0 - i0;
		        i0 = (d_n + (i0 % d_n)) % d_n;
		        int i1 = (i0 + 1) % d_n;
		      
		        int j0 = clamp(y0);
		        fftw_real t = y0 - j0;
		        j0 = (d_n + (j0 % d_n)) % d_n;
		        int j1 = (j0 + 1) % d_n;
		      
		        d_vx[IDX(i,j)] = (1-s) * ((1-t) * d_vx0[IDX(i0, j0)] + t * d_vx0[IDX(i0, j1)]) + s * ((1 - t) * d_vx0[IDX(i1, j0)] + t * d_vx0[IDX(i1, j1)]);
		        d_vy[IDX(i,j)] = (1-s) * ((1-t) * d_vy0[IDX(i0, j0)] + t * d_vy0[IDX(i0, j1)]) + s * ((1 - t) * d_vy0[IDX(i1, j0)] + t * d_vy0[IDX(i1, j1)]);
	  	    }
	  	}
  	}

	for (size_t i = 0; i < d_n; i++)
		for (size_t j = 0; j < d_n; j++)
		{ 
			d_vx0[i + (d_n + 2) * j] = d_vx[IDX(i, j)];
			d_vy0[i + (d_n + 2) * j] = d_vy[IDX(i, j)];
		}

	FFT(1, d_vx0);
	FFT(1, d_vy0);

	for (size_t i = 0; i <= d_n; i += 2)
	{
	    fftw_real x = 0.5f * i;
	    for (size_t j = 0; j < d_n; j++)
	    {
			fftw_real y = j <= d_n / 2
				? (fftw_real) j
				: (fftw_real) j - d_n;
	      
	        fftw_real r = x*x + y*y;
	        if (r == 0.0f)
				continue;

			fftw_real f = (fftw_real) exp(-r * d_dt * d_visc);
			
			fftw_real U[2], V[2];
			U[0] = d_vx0[i  +(d_n+2)*j];
			V[0] = d_vy0[i  +(d_n+2)*j];

			U[1] = d_vx0[i+1+(d_n+2)*j];
			V[1] = d_vy0[i+1+(d_n+2)*j];

	      	d_vx0[i  +(d_n+2)*j] = f*((1-x*x/r)*U[0]     -x*y/r *V[0]);
	        d_vx0[i+1+(d_n+2)*j] = f*((1-x*x/r)*U[1]     -x*y/r *V[1]);
	        d_vy0[i+  (d_n+2)*j] = f*(  -y*x/r *U[0] + (1-y*y/r)*V[0]);
	        d_vy0[i+1+(d_n+2)*j] = f*(  -y*x/r *U[1] + (1-y*y/r)*V[1]);
	   }
	}

	FFT(-1, d_vx0);
	FFT(-1, d_vy0);

	fftw_real f = 1.0 / (d_n * d_n);
 	for (size_t i = 0; i < d_n; i++)
		for (size_t j = 0; j < d_n; j++)
		{
			d_vx[IDX(i,j)] = f * d_vx0[i + (d_n + 2) * j];
			d_vy[IDX(i,j)] = f * d_vy0[i + (d_n + 2) * j];
		}
}

void Simulation::diffuse_matter()
{
	fftw_real x, y;
	size_t i, j;

	for (x = 0.5f / d_n, i = 0; i < d_n; i++, x += 1.0f / d_n)
	{
		for (y = 0.5f / d_n, j = 0 ; j < d_n; j++, y += 1.0f / d_n)
		{
			fftw_real x0 = d_n * (x - d_dt * d_vx[ IDX(i,j)]) - 0.5f;
			fftw_real y0 = d_n * (y - d_dt * d_vy[ IDX(i,j)]) - 0.5f;
			
			int i0 = clamp(x0);
			fftw_real s = x0 - i0;
			i0 = (d_n + (i0 % d_n)) % d_n; // FIXME this is needlessly convoluted (d_n + i0 % d_n) ?
			int i1 = (i0 + 1) % d_n;
			
			int j0 = clamp(y0);
			fftw_real t = y0 - j0;
			j0 = (d_n + (j0 % d_n)) % d_n;
			int j1 = (j0 + 1) % d_n;
			d_rho[IDX(i,j)] = (1 - s) * ((1 - t) * d_rho0[i0 + d_n * j0] + t * d_rho0[i0 + d_n * j1]) + s * ((1 - t) * d_rho0[i1 + d_n * j0] + t * d_rho0[i1 + d_n * j1]);
		}
	}
}

void Simulation::set_forces()
{
	for (size_t i = 0; i < d_n * d_n; i++)
	{
        d_rho0[i] = 0.995 * d_rho[i];
        d_fx[i] *= 0.85;
        d_fy[i] *= 0.85;
        d_vx0[i] = d_fx[i];
        d_vy0[i] = d_fy[i];
	}
}

void Simulation::addForce(int x, int y, double dx, double dy)
{
	d_fx[IDX(x,y)] += dx;
	d_fy[IDX(x,y)] += dy;
	d_rho[IDX(x,y)] = 10.0f;
}

void Simulation::step()
{
	set_forces();
	solve();
	diffuse_matter();
    emit stepped();
}

void Simulation::setViscosity(float visc)
{
    d_visc = visc;
}

void Simulation::setViscosityExp(int power)
{
    setViscosity(pow(10.0, -power));
}

void Simulation::setTimeStep(double dt)
{
    d_dt = dt;
}

void Simulation::setTimeStepInMilliseconds(int milliseconds)
{
    setTimeStep((double) milliseconds / 1000);
}

void Simulation::timerEvent(QTimerEvent *event) {
    Q_UNUSED(event);
    step();
}

float Simulation::rho(size_t i, size_t j) const {
    return d_rho[IDX(i,j)];
}

QVector3D Simulation::v(size_t i, size_t j) const
{
    return QVector3D(d_vx[IDX(i, j)], d_vy[IDX(i, j)], 0.0f);
}

QVector3D Simulation::f(size_t i, size_t j) const
{
    return QVector3D(d_fx[IDX(i, j)], d_fy[IDX(i, j)], 0.0f);
}

QSharedPointer<ScalarView> Simulation::rho() const
{
    return QSharedPointer<ScalarView>(new SimulationScalarView(this, &Simulation::rho));
}

QSharedPointer<VectorView> Simulation::v() const
{
    return QSharedPointer<VectorView>(new SimulationVectorView(this, &Simulation::v));
}

QSharedPointer<VectorView> Simulation::f() const
{
    return QSharedPointer<VectorView>(new SimulationVectorView(this, &Simulation::f));
}

SimulationScalarView::SimulationScalarView(Simulation const *simulation, Simulation::ScalarAccessorFunctionPtr fun)
    : ScalarView(),
      d_sim(simulation),
      d_fun(fun)
{
    // Propagate the simulation's stepped event
    connect(simulation, SIGNAL(stepped()), this, SIGNAL(changed()));
}

QPoint SimulationScalarView::cellAt(int x, int y) const
{
    return QPoint((x % d_sim->d_n + d_sim->d_n) % d_sim->d_n,
                  (y % d_sim->d_n + d_sim->d_n) % d_sim->d_n);
}

float SimulationScalarView::valueAt(int x, int y, int z) const
{
    Q_UNUSED(z);
    return (d_sim->*d_fun)((x % d_sim->d_n + d_sim->d_n) % d_sim->d_n,
                           (y % d_sim->d_n + d_sim->d_n) % d_sim->d_n);
}

QSize SimulationScalarView::size() const
{
    return QSize(d_sim->d_n, d_sim->d_n);
}

size_t SimulationScalarView::depth() const
{
    return 1;
}

SimulationVectorView::SimulationVectorView(Simulation const *simulation, Simulation::VectorAccessorFunctionPtr fun)
    : VectorView(),
      d_sim(simulation),
      d_fun(fun)
{
    connect(simulation, SIGNAL(stepped()), this, SIGNAL(changed()));
}

QPoint SimulationVectorView::cellAt(int x, int y) const
{
    return QPoint(
                (x % d_sim->d_n + d_sim->d_n) % d_sim->d_n,
                (y % d_sim->d_n + d_sim->d_n) % d_sim->d_n);
}

QVector3D SimulationVectorView::valueAt(int x, int y, int z) const
{
    Q_UNUSED(z);
    return (d_sim->*d_fun)((x % d_sim->d_n + d_sim->d_n) % d_sim->d_n,
                           (y % d_sim->d_n + d_sim->d_n) % d_sim->d_n);
}

QSize SimulationVectorView::size() const
{
    return QSize(d_sim->d_n, d_sim->d_n);
}

size_t SimulationVectorView::depth() const
{
    return 1;
}
