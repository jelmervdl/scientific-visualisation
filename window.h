#ifndef WINDOW_H
#define WINDOW_H

#include <QMainWindow>
#include <QMap>
#include <QSharedPointer>
#include "simulation.h"

class QSlider;
class QPushButton;
class QAction;
class ColorMap;
class GLWidget;
class Simulation;
class ColorLegend;

class Window : public QMainWindow
{
    Q_OBJECT

public:
    Window(QWidget *parent = 0);

protected:
    void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;

protected slots:
    void setColorMap(QAction *action);
    void setScalarProperty(QAction *action);
    void setVectorProperty(QAction *action);
    void toggle3D(bool);
    void toggleSimulation(bool);
    void createNewColorMap();

private:
    GLWidget *glWidget;
    QSlider *dtSlider;
    QSlider *viscSlider;
    QAction *toggleAutoScaleAction;
    ColorLegend *colorLegend;
    Simulation *simulation;
    int m_timer;
    QMap<QString, QSharedPointer<ColorMap> > colorMaps;
    QMap<QString, QSharedPointer<ScalarView> > scalarData;
    QMap<QString, QSharedPointer<VectorView> > vectorData;
};

#endif
