#ifndef LINEARCOLORMAP_H
#define LINEARCOLORMAP_H

#include "colormap.h"
#include <QColor>
#include <QVector>
#include <QFile>

class LinearColorMap : public ColorMap
{    
public:
    LinearColorMap();
    LinearColorMap(QVector<QColor> const &stops);
    virtual QColor colorAt(float value) const;

    QVector<QColor> &stops();

    static LinearColorMap *fromFile(QFile &file);

protected:
    QVector<QColor> m_stops;
};

#endif // LINEARCOLORMAP_H
