#ifndef BOUNDSCALARVIEW_H
#define BOUNDSCALARVIEW_H

#include "scalarview.h"
#include <QSharedPointer>

class BoundScalarView : public ScalarView
{
    Q_OBJECT
public:
    BoundScalarView(QSharedPointer<ScalarView const> const &parent, float min, float max);
    QSize size() const;
    size_t depth() const;
    float valueAt(int x, int y, int z) const;
    QPoint cellAt(int x, int y) const;

signals:
    void boundsChanged();

public slots:
    void setMinimum(float value);
    void setMaximum(float value);

private:
    QSharedPointer<ScalarView const> m_parent;
    float m_minimum;
    float m_maximum;
};


#endif // BOUNDSCALARVIEW_H
