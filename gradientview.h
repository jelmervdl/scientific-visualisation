#ifndef GRADIENTVIEW_H
#define GRADIENTVIEW_H

#include "vectorview.h"
#include "scalarview.h"
#include <QObject>
#include <QSharedPointer>

class GradientView : public VectorView
{
    Q_OBJECT
public:
    GradientView(QSharedPointer<ScalarView const> const &parent);
    QSize size() const;
    size_t depth() const;
    QVector3D valueAt(int x, int y, int z = 0) const;
    QPoint cellAt(int x, int y) const;
private:
    QSharedPointer<ScalarView const> m_parent;
};


#endif // GRADIENTVIEW_H
