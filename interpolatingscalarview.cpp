#include "interpolatingscalarview.h"
#include <QDebug>

InterpolatingScalarView::InterpolatingScalarView(QSize size, QSharedPointer<ScalarView const> const &parent)
    : ScalarView(),
      m_size(size),
      m_parent(parent)
{
    connect(parent.data(), SIGNAL(changed()), this, SIGNAL(changed()));
}

float InterpolatingScalarView::valueAt(int x, int y, int z) const
{
    // Scale 0-1 to 0-n
    float ax = (float) ((x % m_size.width() + m_size.width()) % m_size.width()) / m_size.width() * (m_parent->size().width() - 1);
    float ay = (float) ((y % m_size.height() + m_size.height()) % m_size.height()) / m_size.height() * (m_parent->size().height() - 1);

    // Floor to the nearest grid cell
    int i = (int) ax;
    int j = (int) ay;

    // how much of that cell do we need,
    // and how much of the neighbour?
    float lx = ax - i;
    float ly = ay - j;

    Q_ASSERT(i >= 0);
    Q_ASSERT(i < m_parent->size().width());
    Q_ASSERT(j >= 0);
    Q_ASSERT(j < m_parent->size().height());

    // Perform bilinear interpolation
    return m_parent->valueAt(i, j, z) * (1.0f - lx) * (1.0f - ly)
            + m_parent->valueAt(i + 1, j, z) * lx * (1.0f - ly)
            + m_parent->valueAt(i, j + 1, z) * (1.0f - lx) * ly
            + m_parent->valueAt(i + 1, j + 1, z) * lx * ly;
}

QPoint InterpolatingScalarView::cellAt(int x, int y) const
{
    // Scale 0-1 to 0-n
    float ax = (float) ((x % m_size.width() + m_size.width()) % m_size.width()) / m_size.width() * m_parent->size().width();
    float ay = (float) ((y % m_size.height() + m_size.height()) % m_size.height()) * m_parent->size().height();

    // Floor to the nearest grid cell
    return QPoint((int) ax, (int) ay);
}

QSize InterpolatingScalarView::size() const
{
    return m_size;
}

size_t InterpolatingScalarView::depth() const
{
    return m_parent->depth();
}
