#ifndef INTERPOLATINGSCALARVIEW_H
#define INTERPOLATINGSCALARVIEW_H

#include "scalarview.h"
#include <QObject>
#include <QSharedPointer>

class InterpolatingScalarView: public ScalarView
{
    Q_OBJECT
public:
    InterpolatingScalarView(QSize size, QSharedPointer<ScalarView const> const &parent);
    QSize size() const;
    size_t depth() const;
    float valueAt(int x, int y, int z = 0) const;
    QPoint cellAt(int x, int y) const;
private:
    QSize m_size;
    QSharedPointer<ScalarView const> m_parent;
};

#endif // INTERPOLATINGSCALARVIEW_H
