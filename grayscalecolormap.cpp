#include "grayscalecolormap.h"

QColor GrayscaleColorMap::colorAt(float value) const
{
    value = normalize(value);
    return QColor(value * 255, value * 255, value * 255);
}
