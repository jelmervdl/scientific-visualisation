#ifndef ZEBRACOLORMAP_H
#define ZEBRACOLORMAP_H

#include "colormap.h"
#include <QList>

class ZebraColorMap : public ColorMap
{
public:
    ZebraColorMap(int patches, QList<QColor> const &colors);
    virtual QColor colorAt(float val) const;
 protected:
    int m_patches;
    QList<QColor> m_colors;
};


#endif // ZEBRACOLORMAP_H
