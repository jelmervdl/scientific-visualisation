#include "glwidget.h"
#include "window.h"
#include <QAction>
#include <QActionGroup>
#include <QMenuBar>
#include <QSlider>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QKeyEvent>
#include <QPushButton>
#include <QDesktopWidget>
#include <QApplication>
#include <QMessageBox>
#include <QFile>
#include <QDir>
#include <QCoreApplication>
#include <QColorDialog>
#include "simulation.h"
#include "colormap.h"
#include "grayscalecolormap.h"
#include "rainbowcolormap.h"
#include "linearcolormap.h"
#include "huecolormap.h"
#include "zebracolormap.h"
#include "colorlegend.h"
#include "scalarview.h"
#include "interpolatingscalarview.h"
#include "magnitudeview.h"
#include "divergenceview.h"
#include "gradientview.h"
#include "subsamplingvectorview.h"
#include "boundvectorview.h"
#include "boundscalarview.h"
#include "normalisedscalarview.h"
#include "floatslider.h"
#include "scalarvolumeview.h"

Window::Window(QWidget *parent)
    :
      QMainWindow(parent),
      m_timer(-1)
{
    const int resolution = 100;

    simulation = new Simulation(resolution, 0.4, 0.001);
    toggleSimulation(true);

    QSharedPointer<ScalarView const> rho(simulation->rho());

    QSharedPointer<ScalarVolumeView const> rhoHist(new ScalarVolumeView(rho, 5, 5));

    glWidget = new GLWidget(this);
    glWidget->setData(rhoHist);
    glWidget->setDrawSmoke(true);
    glWidget->setDrawArrows(false);
    connect(glWidget, SIGNAL(mouseDrawing(int, int, double, double)), simulation, SLOT(addForce(int, int, double, double)));

    colorLegend = new ColorLegend(Qt::Vertical, this);
    connect(colorLegend, SIGNAL(stopsChanged(QVector<float>)), glWidget, SLOT(setIsolines(QVector<float>)));

    dtSlider = new FloatSlider(Qt::Vertical);
    dtSlider->setToolTip(tr("Time step"));
    dtSlider->setRange(0, 1000);
    dtSlider->setTickPosition(QSlider::TicksRight);
    connect(dtSlider, SIGNAL(valueChanged(int)), simulation, SLOT(setTimeStepInMilliseconds(int)));

    viscSlider = new QSlider(Qt::Vertical);
    viscSlider->setToolTip(tr("Viscosity"));
    viscSlider->setRange(0, 10);
    viscSlider->setTickPosition(QSlider::TicksRight);
    connect(viscSlider, SIGNAL(valueChanged(int)), simulation, SLOT(setViscosityExp(int)));

    FloatSlider *arrowBaseSlider = new FloatSlider(Qt::Vertical, this);
    arrowBaseSlider->setToolTip(tr("Arrow base size"));
    arrowBaseSlider->setMaximum(20);
    arrowBaseSlider->setScale(1.0f / 40);
    arrowBaseSlider->setScaledValue(glWidget->arrowBase());
    connect(arrowBaseSlider, SIGNAL(valueChanged(float)), glWidget, SLOT(setArrowBase(float)));

    FloatSlider *arrowScaleSlider = new FloatSlider(Qt::Vertical, this);
    arrowScaleSlider->setToolTip(tr("Arrow scale"));
    arrowScaleSlider->setMaximum(100);
    arrowScaleSlider->setScale(1.0f / 100);
    arrowScaleSlider->setScaledValue(glWidget->arrowScale());
    connect(arrowScaleSlider, SIGNAL(valueChanged(float)), glWidget, SLOT(setArrowScale(float)));

    QWidget *centralWidget = new QWidget(this);

    QVBoxLayout *mainLayout = new QVBoxLayout;
    QHBoxLayout *container = new QHBoxLayout;
    container->addWidget(glWidget);
    container->addWidget(colorLegend);
    container->addWidget(dtSlider);
    container->addWidget(viscSlider);
    container->addWidget(arrowBaseSlider);
    container->addWidget(arrowScaleSlider);

    QWidget *w = new QWidget;
    w->setLayout(container);
    mainLayout->addWidget(w);

    centralWidget->setLayout(mainLayout);
    setCentralWidget(centralWidget);

    dtSlider->setValue(400);
    viscSlider->setValue(4);

    colorMaps["Grayscale"] = QSharedPointer<ColorMap>(new GrayscaleColorMap());
    colorMaps["Rainbow"] = QSharedPointer<ColorMap>(new RainbowColorMap());
    colorMaps["Hue Green"] = QSharedPointer<ColorMap>(new HueColorMap(QColor::fromHsv(120, 255, 0)));
    colorMaps["Zebra B&W"] = QSharedPointer<ColorMap>(new ZebraColorMap(10, {QColor(Qt::black), QColor(Qt::white)}));
    colorMaps["Zebra Dutch"] = QSharedPointer<ColorMap>(new ZebraColorMap(5, {QColor(Qt::red), QColor(Qt::white), QColor(Qt::blue), QColor(Qt::black)}));

    scalarData["Fluid Density"] = QSharedPointer<ScalarView>(simulation->rho());
    scalarData["Velocity"] = QSharedPointer<ScalarView>(new MagnitudeView(simulation->v()));
    scalarData["Force Field"] = QSharedPointer<ScalarView>(new MagnitudeView(simulation->f()));
    scalarData["Divergence of Velocity"] = QSharedPointer<ScalarView>(new DivergenceView(simulation->v()));
    scalarData["Divergence of Force"] = QSharedPointer<ScalarView>(new DivergenceView(simulation->f()));

    vectorData["Velocity"] = QSharedPointer<VectorView>(simulation->v());
    vectorData["Force Field"] = QSharedPointer<VectorView>(simulation->f());
    vectorData["Gradient of Density"] = QSharedPointer<VectorView>(new GradientView(simulation->rho()));

    QDir colorMapFolder(QString("%1/../Resources/Colormaps").arg(QCoreApplication::applicationDirPath()));
    colorMapFolder.setFilter(QDir::Files | QDir::Hidden | QDir::NoSymLinks);
    colorMapFolder.setNameFilters(QStringList("*.colormap"));
    QFileInfoList colorMapFiles = colorMapFolder.entryInfoList();
    for (int i = 0; i < colorMapFiles.size(); ++i) {
        QFile colorMapFile(colorMapFiles.at(i).absoluteFilePath());
        QSharedPointer<ColorMap> colorMap(LinearColorMap::fromFile(colorMapFile));
        if (colorMap != 0)
            colorMaps[colorMapFiles.at(i).baseName()] = colorMap;
    }

    QMenuBar *menuBar = new QMenuBar;

    QMenu *simulationMenu = menuBar->addMenu(tr("Simulation"));

    QAction *toggleSimulationAction = simulationMenu->addAction(tr("Run Simulation"));
    toggleSimulationAction->setCheckable(true);
    toggleSimulationAction->setChecked(true);
    connect(toggleSimulationAction, SIGNAL(toggled(bool)), this, SLOT(toggleSimulation(bool)));

    QMenu *viewMenu = menuBar->addMenu(tr("&View"));

    // Color map submenu

    QMenu *colorMapMenu = viewMenu->addMenu(tr("Color Map"));
    QActionGroup *colorMapMenuGroup = new QActionGroup(colorMapMenu);
    colorMapMenuGroup->setExclusive(true);
    connect(colorMapMenuGroup, SIGNAL(triggered(QAction*)), this, SLOT(setColorMap(QAction*)));

    foreach (QString const &name, colorMaps.keys()) {
        QAction *colorMapMenuItem = colorMapMenuGroup->addAction(name);
        colorMapMenuItem->setCheckable(true);
        colorMapMenuItem->setData(name);
    }

    colorMapMenu->addActions(colorMapMenuGroup->actions());

    colorMapMenu->addSeparator();

    QAction *addColorMapAction = colorMapMenu->addAction(tr("Custom Color Map…"));
    connect(addColorMapAction, SIGNAL(triggered(bool)), this, SLOT(createNewColorMap()));

    colorLegend->setColorMap(colorMaps["Rainbow"]);
    glWidget->setColorMap(colorMaps["Rainbow"]);

    // Smoke & Scalar property visualisation

    viewMenu->addSection(tr("Scalar data"));

    QAction *toggleSmoke = viewMenu->addAction(tr("Draw Smoke"));
    toggleSmoke->setCheckable(true);
    connect(toggleSmoke, SIGNAL(toggled(bool)), glWidget, SLOT(setDrawSmoke(bool)));

    QActionGroup *scalarPropertyGroup = new QActionGroup(viewMenu);
    scalarPropertyGroup->setExclusive(true);
    connect(scalarPropertyGroup, SIGNAL(triggered(QAction*)), this, SLOT(setScalarProperty(QAction*)));
    connect(toggleSmoke, SIGNAL(toggled(bool)), scalarPropertyGroup, SLOT(setEnabled(bool)));

    foreach (QString const &prop, scalarData.keys()) {
        QAction *action = scalarPropertyGroup->addAction(prop);
        action->setCheckable(true);
        action->setData(prop);
    }

    toggleSmoke->setChecked(glWidget->drawingSmoke());
    scalarPropertyGroup->setEnabled(glWidget->drawingSmoke());

    viewMenu->addActions(scalarPropertyGroup->actions());

    // Arrows and vector property visualisation

    viewMenu->addSection(tr("Vector data"));

    QAction *toggleArrows = viewMenu->addAction(tr("Draw Arrows"));
    toggleArrows->setCheckable(true);
    connect(toggleArrows, SIGNAL(toggled(bool)), glWidget, SLOT(setDrawArrows(bool)));

    QActionGroup *vectorPropertyGroup = new QActionGroup(viewMenu);
    vectorPropertyGroup->setExclusive(true);
    connect(vectorPropertyGroup, SIGNAL(triggered(QAction*)), this, SLOT(setVectorProperty(QAction *)));
    connect(toggleArrows, SIGNAL(toggled(bool)), vectorPropertyGroup, SLOT(setEnabled(bool)));

    foreach (QString const &prop, vectorData.keys()) {
        QAction *action = vectorPropertyGroup->addAction(prop);
        action->setCheckable(true);
        action->setData(prop);
    }

    toggleArrows->setChecked(glWidget->drawingArrows());
    vectorPropertyGroup->setEnabled(glWidget->drawingArrows());

    viewMenu->addActions(vectorPropertyGroup->actions());

    viewMenu->addSeparator();

    QAction *toggle3D = viewMenu->addAction(tr("3D Height Map"));
    toggle3D->setCheckable(true);
    connect(toggle3D, SIGNAL(toggled(bool)), this, SLOT(toggle3D(bool)));

    toggleAutoScaleAction = viewMenu->addAction(tr("Auto Scale"));
    toggleAutoScaleAction->setCheckable(true);

    setWindowTitle(tr("Melting Pot"));
    setMenuBar(menuBar);
}

void Window::keyPressEvent(QKeyEvent *e)
{
    if (e->key() == Qt::Key_Escape)
        close();
    else
        QWidget::keyPressEvent(e);
}

void Window::setColorMap(QAction *action)
{
    QSharedPointer<ColorMap> colorMap = colorMaps[action->data().toString()];
    glWidget->setColorMap(colorMap);
    colorLegend->setColorMap(colorMap);
}

void Window::setScalarProperty(QAction *action)
{
    QSharedPointer<ScalarView> value = scalarData[action->data().toString()];
    value = QSharedPointer<ScalarView>(new BoundScalarView(value, -3, 3));

    if (toggleAutoScaleAction->isChecked()) {
        QSharedPointer<NormalisedScalarView> normed(new NormalisedScalarView(value));
        connect(normed.data(), SIGNAL(scaleChanged(float)), colorLegend, SLOT(setScale(float)));
        value = normed;
    }

    glWidget->setData(value);
}

void Window::setVectorProperty(QAction *action)
{
    QSharedPointer<VectorView> direction(new SubsamplingVectorView(QSize(25, 25), vectorData[action->data().toString()]));
    QSharedPointer<ScalarView> value(new MagnitudeView(direction));
    QSharedPointer<ScalarView> normed(new NormalisedScalarView(value));
    QSharedPointer<VectorView> bound(new BoundVectorView(direction, 0.1, 1.0));

    glWidget->setArrowDirection(bound);
    glWidget->setArrowValue(normed);
}

void Window::toggle3D(bool enable)
{
    if (enable) {
        glWidget->setRenderDepth(0.5f);
        glWidget->camera().tilt(65);
    } else {
        glWidget->setRenderDepth(0.f);
        glWidget->camera().tilt(0);
        glWidget->camera().rotate(0);
    }
}

void Window::createNewColorMap()
{
    QColor baseColor = QColorDialog::getColor();

    if (baseColor.isValid()) {
        QSharedPointer<ColorMap> colorMap(new HueColorMap(baseColor));
        glWidget->setColorMap(colorMap);
        colorLegend->setColorMap(colorMap);
    }
}

void Window::toggleSimulation(bool enable)
{
    if (enable && m_timer == -1)
        m_timer = simulation->startTimer(50);
    else {
        simulation->killTimer(m_timer);
        m_timer = -1;
    }
}
