#ifndef DIVERGENCEVIEW_H
#define DIVERGENCEVIEW_H

#include "scalarview.h"
#include "vectorview.h"
#include <QObject>
#include <QSharedPointer>

class DivergenceView : public ScalarView
{
    Q_OBJECT
public:
    DivergenceView(QSharedPointer<VectorView const> const &parent);
    QSize size() const;
    size_t depth() const;
    float valueAt(int x, int y, int z = 0) const;
    QPoint cellAt(int x, int y) const;
private:
    QSharedPointer<VectorView const> m_parent;
};

#endif // DIVERGENCEVIEW_H
