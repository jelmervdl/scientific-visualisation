#include "scalarvolumeview.h"
#include <QDebug>

#define IDX(x, y, z) (m_offset \
    + z * m_frameSize \
    + y * m_size.width() \
    + x) % (m_frameSize * m_stride * m_depth)

ScalarVolumeView::ScalarVolumeView(QSharedPointer<ScalarView const> const &source, size_t depth, size_t stride, QObject *parent)
    : ScalarView(parent),
      m_source(source),
      m_size(source->size()), // Saved for performance reasons
      m_depth(depth),
      m_stride(stride),
      m_frameCount(0),
      m_frameSize(m_size.width() * m_size.height()),
      m_offset(0)
{
    connect(source.data(), SIGNAL(changed()), this, SLOT(captureFrame()));
    m_data = new float[m_frameSize * m_stride * m_depth]();
}

ScalarVolumeView::~ScalarVolumeView()
{
    delete m_data;
}

QSize ScalarVolumeView::size() const
{
    return m_size;
}

size_t ScalarVolumeView::depth() const
{
    return m_depth;
}

float ScalarVolumeView::valueAt(int x, int y, int z) const
{
    // Note that we move the offset, not the stride!
    return m_data[IDX(x, y, m_stride * z)];
}

QPoint ScalarVolumeView::cellAt(int x, int y) const
{
    return m_source->cellAt(x, y);
}

void ScalarVolumeView::captureFrame()
{
    // Move offset one frame to the left/up
    m_offset = (((int) m_offset - m_frameSize) + (m_frameSize * m_stride * m_depth)) % (m_frameSize * m_stride * m_depth);

    for (int y = 0; y < m_size.height(); ++y)
        for (int x = 0; x < m_size.width(); ++x)
            m_data[IDX(x, y, 0)] = m_source->valueAt(x, y);

    emit changed();
}
