#include "rainbowcolormap.h"
#include <QDebug>
#include <cmath>

QColor RainbowColorMap::colorAt(float value) const
{
    const float dx = 0.8f;

    value = (6  -2 * dx) * normalize(value) + dx;

    int r = 255.0f * fmax(0.0f,(3-(float)fabs(value-4)-(float)fabs(value-5))/2);
    int g = 255.0f * fmax(0.0f,(4-(float)fabs(value-2)-(float)fabs(value-4))/2);
    int b = 255.0f * fmax(0.0f,(3-(float)fabs(value-1)-(float)fabs(value-2))/2);

    return QColor(r,g,b);
}
