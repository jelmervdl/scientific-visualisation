#include "subsamplingvectorview.h"
#include <QDebug>

SubsamplingVectorView::SubsamplingVectorView(QSize size, QSharedPointer<VectorView const> const &parent)
    : VectorView(),
      m_size(size),
      m_parent(parent)
{
    if (parent->size().width() % size.width() != 0 || parent->size().height() % size.height() != 0)
        qWarning() << "Creating a subsampling view of a vector view that is not N times the size of the subsampling view.";

    connect(parent.data(), SIGNAL(changed()), this, SIGNAL(changed()));
}

QVector3D SubsamplingVectorView::valueAt(int x, int y, int z) const
{
    int cell_width = m_parent->size().width() / m_size.width();
    int cell_height = m_parent->size().height() / m_size.height();

    // Gradient
    QVector3D sum;

    for (int i = -cell_width / 2; i < cell_width / 2; ++i) {
        for (int j = -cell_width / 2; j < cell_width / 2; ++j) {
            sum += m_parent->valueAt(cell_width * x + i, cell_height * y + j, z);
        }
    }

    // Divergence
    return sum / (cell_width * cell_height);
}

QPoint SubsamplingVectorView::cellAt(int x, int y) const
{
    int cell_width = m_parent->size().width() / m_size.width();
    int cell_height = m_parent->size().height() / m_size.height();

    return m_parent->cellAt(cell_width * x + cell_width / 2, cell_height * y + cell_height / 2);
}

QSize SubsamplingVectorView::size() const
{
    return m_size;
}

size_t SubsamplingVectorView::depth() const
{
    return m_parent->depth();
}
