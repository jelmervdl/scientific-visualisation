#ifndef BOUNDVECTORVIEW_H
#define BOUNDVECTORVIEW_H

#include "vectorview.h"
#include <QSharedPointer>

class BoundVectorView : public VectorView
{
    Q_OBJECT
public:
    BoundVectorView(QSharedPointer<VectorView const> const &parent, float min, float max);
    QSize size() const;
    size_t depth() const;
    QVector3D valueAt(int x, int y, int z = 0) const;
    QPoint cellAt(int x, int y) const;

signals:
    void boundsChanged();

public slots:
    void setMinimum(float value);
    void setMaximum(float value);

private:
    QSharedPointer<VectorView const> m_parent;
    float m_minimum;
    float m_maximum;
};

#endif // BOUNDVECTORVIEW_H
