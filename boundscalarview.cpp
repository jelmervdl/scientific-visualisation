#include "boundscalarview.h"

BoundScalarView::BoundScalarView(QSharedPointer<ScalarView const> const &parent, float minimum, float maximum)
    : ScalarView(),
      m_parent(parent),
      m_minimum(minimum),
      m_maximum(maximum)
{
    Q_ASSERT(minimum < maximum);
    connect(parent.data(), SIGNAL(changed()), this, SIGNAL(changed()));
    connect(this, SIGNAL(boundsChanged()), this, SIGNAL(changed()));
}

void BoundScalarView::setMinimum(float value)
{
    m_minimum = value;
    emit boundsChanged();
}

void BoundScalarView::setMaximum(float value)
{
    m_maximum = value;
    emit boundsChanged();
}

float BoundScalarView::valueAt(int x, int y, int z) const
{
    return qBound(m_minimum, m_parent->valueAt(x, y, z), m_maximum);
}

QPoint BoundScalarView::cellAt(int x, int y) const
{
    return m_parent->cellAt(x, y);
}

QSize BoundScalarView::size() const
{
    return m_parent->size();
}

size_t BoundScalarView::depth() const
{
    return m_parent->depth();
}
