#ifndef COLORLEGEND_H
#define COLORLEGEND_H

#include <QWidget>
#include <QSharedPointer>
#include "colormap.h"

class QPaintEvent;

class ColorLegend : public QWidget
{
    Q_OBJECT
public:
    explicit ColorLegend(Qt::Orientation orientation, QWidget *parent = 0);
    void setColorMap(QSharedPointer<ColorMap const> const &colorMap);

    QSize minimumSizeHint() const Q_DECL_OVERRIDE;
    QSize sizeHint() const Q_DECL_OVERRIDE;
    QVector<float> const &stops() const;

public slots:
    void setScale(float scale);

protected:
    virtual void paintEvent(QPaintEvent *event) Q_DECL_OVERRIDE;
    virtual void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    virtual void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    virtual void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    bool event(QEvent *event) Q_DECL_OVERRIDE;
    float valueAt(float position) const;

signals:
    void stopAdded(float position);
    void stopRemoved(int index);
    void stopChanged(int index, float position);
    void stopsChanged(QVector<float> const &stops);

public slots:

private:
    QSharedPointer<ColorMap const> m_colorMap;
    Qt::Orientation m_orientation;
    float m_scale;
    int m_barWidth;

    QVector<float> m_stops;
    int m_selectedStopIndex;
    QPoint m_mousePressPos;
};

#endif // COLORLEGEND_H
