#include "scaledscalarview.h"

ScaledScalarView::ScaledScalarView(QSharedPointer<ScalarView const> const &parent, float scale)
    : ScalarView(),
      m_parent(parent),
      m_scale(scale)
{
    connect(parent.data(), SIGNAL(changed()), this, SIGNAL(changed()));
    connect(this, SIGNAL(scaleChanged(float)), this, SIGNAL(changed()));
}

float ScaledScalarView::scale() const
{
    return m_scale;
}

void ScaledScalarView::setScale(float scale)
{
    m_scale = scale;
    emit scaleChanged(m_scale);
}

float ScaledScalarView::valueAt(int x, int y, int z) const
{
    return m_scale * m_parent->valueAt(x, y, z);
}

QPoint ScaledScalarView::cellAt(int x, int y) const
{
    return m_parent->cellAt(x, y);
}

QSize ScaledScalarView::size() const
{
    return m_parent->size();
}

size_t ScaledScalarView::depth() const
{
    return m_parent->depth();
}
