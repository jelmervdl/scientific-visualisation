#version 150
in highp vec3 vert;
in highp float geomVal;
out highp vec4 fragColor;
uniform sampler2D colormap;

void main() {
    fragColor = texture(colormap, vec2(geomVal, 0));
}
