#version 150
in highp vec3 vert;
in highp float val;
in highp vec3 norm;
out highp vec4 fragColor;
uniform sampler2D colormap;
uniform vec3 lightPos;
uniform float renderDepth;
uniform int layerCount;

vec3 L, color, col;
float NL;

void main() {
    color = texture(colormap, vec2(val, 0)).xyz;

    if (renderDepth > 0.1) {
        L = normalize(lightPos - vert.xyz);
        NL = max(dot(normalize(norm), L), 0.0);
        col = clamp(color * 0.2 + color * 0.8 * NL, 0.0, 1.0);
    } else {
        col = color;
    }
    fragColor = vec4(col, 1.0/float(layerCount));
}
