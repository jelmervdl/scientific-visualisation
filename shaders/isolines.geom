#version 150

layout(triangles) in;
layout(line_strip, max_vertices = 2) out;

// input position and color
in vec3 vert[];
in float val[];

uniform mat4 projMatrix;
uniform mat4 mvMatrix;
uniform mat3 normalMatrix;
uniform int layerIndex;
uniform int layerCount;
uniform float t;


out gl_PerVertex {
    vec4 gl_Position;
};

out float geomVal;

void main() {
    for (int i = 0; i < 3; ++i) {
        if (val[i] < t && val[(i + 1) % 3] > t) {
            geomVal = (1 - (t - val[i]) / (val[(i + 1) % 3] - val[i])) * val[i] + (t - val[i]) / (val[(i + 1) % 3] - val[i]) * val[(i + 1) % 3];
            gl_Position = projMatrix * mvMatrix * vec4(vert[i] + (vert[(i + 1) % 3] - vert[i]) * (t - val[i]) / (val[(i + 1) % 3] - val[i]), 1.0);
            EmitVertex();
        }
        else if (val[i] > t && val[(i + 1) % 3] < t) {
            geomVal = (1 - (t - val[i]) / (val[(i + 1) % 3] - val[i])) * val[i] + (t - val[i]) / (val[(i + 1) % 3] - val[i]) * val[(i + 1) % 3];
            gl_Position = projMatrix * mvMatrix * vec4(vert[(i + 1) % 3] + (vert[i] - vert[(i + 1) % 3]) * (t - val[(i + 1) % 3]) / (val[i] - val[(i + 1) % 3]), 1.0);
            EmitVertex();
        }
    }

    EndPrimitive();
}
