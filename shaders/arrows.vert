#version 150
in vec2 vertex;
in float value;
out vec4 vert;
out float val;
uniform mat4 projMatrix;
uniform mat4 mvMatrix;
uniform mat3 normalMatrix;
uniform float renderDepth;

void main() {
    vert = vec4(vertex, renderDepth, 1);
    val = value;
    gl_Position = projMatrix * mvMatrix * vert;
}
