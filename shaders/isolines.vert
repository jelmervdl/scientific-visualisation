#version 150
in vec3 vertex;
in float value;
out vec3 vert;
out float val;
uniform mat4 projMatrix;
uniform mat4 mvMatrix;
uniform mat3 normalMatrix;
uniform int layerIndex;
uniform int layerCount;

void main() {
    vert = vec3(vertex.xy, (1.0 - float(layerIndex + 1) / float(layerCount)) / 2.0);
    val = value;
    gl_Position = projMatrix * mvMatrix * vec4(vert, 1.0);
}
