#version 150
in highp float geomVal;
in highp vec3 geomVert;
in highp vec4 geomNormal;
out highp vec4 fragColor;

uniform vec3 lightPos;
uniform sampler2D colormap;

vec3 L, color, col;
float NL;

void main() {
    color = texture(colormap, vec2(geomVal, 0)).xyz;
    L = normalize(lightPos - geomVert.xyz);
    NL = max(dot(normalize(geomNormal.xyz), L), 0.0);
    col = clamp(color * 0.2 + color * 0.8 * NL, 0.0, 1.0);
    fragColor = vec4(col, 1.0);

    //fragColor = texture(colormap, vec2(geomVal, 0));
    //fragColor = vec4(geomNormal.xyz, 1.0);
}
