#version 150
in vec3 vertex;
in float value;
in vec2 difference;
out vec3 vert;
out vec3 norm;
out float val;
uniform mat4 projMatrix;
uniform mat4 mvMatrix;
uniform mat3 normalMatrix;
uniform float renderDepth;
uniform int layerIndex;
uniform int layerCount;


void main() {
    vert = vec3(vertex.xy, renderDepth * value);
    val = value;
    norm = vec3(difference.x, difference.y, renderDepth);
    gl_Position = projMatrix * mvMatrix * vec4(vert.xy, (1.0 - float(layerIndex + 1) / float(layerCount)) / 2.0 + renderDepth * value, 1.0);
}
