#version 150

layout(lines) in;
layout(triangle_strip, max_vertices = 9) out;

// input position and color
in vec4 vert[];
in float val[];

uniform mat4 projMatrix;
uniform mat4 mvMatrix;
uniform mat3 normalMatrix;
uniform float arrowBase;


out gl_PerVertex {
    vec4 gl_Position;
};

out vec3 geomVert;
out vec4 geomNormal;
out float geomVal;

//         G
//       /   \
//      /     \
//     /       \
//    F_________E
//      D     C
//      |   / |
//      | /   |
//      A__X__B
// Known:
// X = vert[0]
// G = vert[1]
// AX and BX are arrowBase
// DF and CE are arrowBase as well
// dir = X->G
// norm = X->A


vec3 dir;
vec3 norm;

void main() {
    dir = vert[1].xyz - vert[0].xyz;
    norm = vec3(dir.y, -dir.x, 0);

    geomVal = val[0];

    // Point of arrow  (G)
    gl_Position = projMatrix * mvMatrix * vec4(vert[1].xyz, 1.0);
    geomVert = vert[1].xyz;
    geomNormal = projMatrix * mvMatrix * vec4(0, 0, 0, 1);
    EmitVertex();

    // Left bottom (F)
    gl_Position = projMatrix * mvMatrix * vec4(vert[0].xyz + 2 * arrowBase * norm.xyz + 0.5 * dir, 1.0);
    geomVert = vert[0].xyz + 2 * arrowBase * norm.xyz + 0.5 * dir;
    geomNormal = projMatrix * mvMatrix * vec4(normalize(norm), 1.0);
    EmitVertex();

    // Right bottom (E)
    gl_Position = projMatrix * mvMatrix * vec4(vert[0].xyz - 2 * arrowBase * norm.xyz + 0.5 * dir, 1.0);
    geomVert = vert[0].xyz - 2 * arrowBase * norm.xyz + 0.5 * dir;
    geomNormal = projMatrix * mvMatrix * vec4(normalize(-norm), 1.0);
    EmitVertex();

    // Emit the whole arrow head (EFG)
    EndPrimitive();

    // bottom left arrow base (A)
    gl_Position = projMatrix * mvMatrix * vec4(vert[0].xyz + arrowBase * norm.xyz, 1.0);
    geomVert = vert[0].xyz + arrowBase * norm.xyz;
    geomNormal = projMatrix * mvMatrix * vec4(normalize(norm), 1.0);
    EmitVertex();

    // top left arrow base (D)
    gl_Position = projMatrix * mvMatrix * vec4(vert[0].xyz + arrowBase * norm.xyz + 0.5 * dir, 1.0);
    geomVert = vert[0].xyz + arrowBase * norm.xyz + 0.5 * dir;
    geomNormal = projMatrix * mvMatrix * vec4(normalize(norm), 1.0);
    EmitVertex();

    // top right arrow base (C)
    gl_Position = projMatrix * mvMatrix * vec4(vert[0].xyz - arrowBase * norm.xyz + 0.5 * dir, 1.0);
    geomVert = vert[0].xyz - arrowBase * norm.xyz + 0.5 * dir;
    geomNormal = projMatrix * mvMatrix * vec4(normalize(-norm), 1.0);
    EmitVertex();

    EndPrimitive(); // ACD

    // bottom left arrow base (A)
    gl_Position = projMatrix * mvMatrix * vec4(vert[0].xyz + arrowBase * norm.xyz, 1.0);
    geomVert = vert[0].xyz + arrowBase * norm.xyz;
    geomNormal = projMatrix * mvMatrix * vec4(normalize(norm), 1.0);
    EmitVertex();

    // top right arrow base (C)
    gl_Position = projMatrix * mvMatrix * vec4(vert[0].xyz - arrowBase * norm.xyz + 0.5 * dir, 1.0);
    geomVert = vert[0].xyz - arrowBase * norm.xyz + 0.5 * dir;
    geomNormal = projMatrix * mvMatrix * vec4(normalize(-norm), 1.0);
    EmitVertex();

    // bottom right arrow base (B)
    gl_Position = projMatrix * mvMatrix * vec4(vert[0].xyz - arrowBase * norm.xyz, 1.0);
    geomVert = vert[0].xyz - arrowBase * norm.xyz;
    geomNormal = projMatrix * mvMatrix * vec4(normalize(-norm), 1.0);
    EmitVertex();

    EndPrimitive(); // ABC
}
