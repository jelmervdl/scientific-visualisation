#include "huecolormap.h"

HueColorMap::HueColorMap(QColor const &baseColor):
    ColorMap(),
    m_baseColor(baseColor)
{
    //
}

QColor HueColorMap::colorAt(float value) const
{
    return QColor::fromHsvF(m_baseColor.hueF(), m_baseColor.saturationF(), normalize(value));
}
