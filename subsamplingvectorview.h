#ifndef SUBSAMPLINGVECTORVIEW_H
#define SUBSAMPLINGVECTORVIEW_H

#include "vectorview.h"
#include <QSharedPointer>

class SubsamplingVectorView : public VectorView
{
    Q_OBJECT
public:
    SubsamplingVectorView(QSize size, QSharedPointer<VectorView const> const &parent);
    QSize size() const;
    size_t depth() const;
    QVector3D valueAt(int x, int y, int z = 0) const;
    QPoint cellAt(int x, int y) const;
private:
    QSize m_size;
    QSharedPointer<VectorView const> m_parent;
};

#endif // SUBSAMPLINGVECTORVIEW_H
