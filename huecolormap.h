#ifndef HUECOLORMAP_H
#define HUECOLORMAP_H

#include "colormap.h"

class HueColorMap : public ColorMap
{
public:
    HueColorMap(QColor const &baseColor);
    virtual QColor colorAt(float val) const;
 protected:
    QColor m_baseColor;
};

#endif // HUECOLORMAP_H
