#include "floatslider.h"

FloatSlider::FloatSlider(QWidget *parent)
    : QSlider(parent),
      m_scale(1.0f)
{
    connect(this, SIGNAL(valueChanged(int)), this, SLOT(updateFloatValue(int)));
}

FloatSlider::FloatSlider(Qt::Orientation orientation, QWidget *parent)
    : QSlider(orientation, parent),
      m_scale(1.0f)
{
    connect(this, SIGNAL(valueChanged(int)), this, SLOT(updateFloatValue(int)));
}

float FloatSlider::scale() const
{
    return m_scale;
}

void FloatSlider::setScale(float scale)
{
    m_scale = scale;
}

float FloatSlider::scaledValue() const
{
    return (int) value() * m_scale;
}

void FloatSlider::setScaledValue(float value)
{
    setValue((int) (value / m_scale));
}

void FloatSlider::updateFloatValue(int value)
{
    emit valueChanged(value * m_scale);
}
