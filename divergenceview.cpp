#include "DivergenceView.h"

DivergenceView::DivergenceView(QSharedPointer<VectorView const> const &parent)
    : ScalarView(),
      m_parent(parent)
{
    connect(parent.data(), SIGNAL(changed()), this, SIGNAL(changed()));
}

float DivergenceView::valueAt(int x, int y, int z) const
{
    // Gradient
    float dx = m_parent->valueAt(x - 1, y, z).x() - m_parent->valueAt(x + 1, y, z).x();
    float dy = m_parent->valueAt(x, y - 1, z).y() - m_parent->valueAt(x, y + 1, z).y();

    // Divergence
    return dx + dy;
}

QPoint DivergenceView::cellAt(int x, int y) const
{
    return m_parent->cellAt(x, y);
}

QSize DivergenceView::size() const
{
    return m_parent->size();
}

size_t DivergenceView::depth() const
{
    return m_parent->depth();
}
