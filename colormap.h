#ifndef COLORMAP_H
#define COLORMAP_H

#include <QObject>
#include <QColor>
#include <QVector>
#include <QPair>
#include <gltypes.h>

class ColorMap : public QObject
{
    Q_OBJECT
public:
    ColorMap();
    virtual QColor colorAt(float val) const = 0;
    QVector<GLfloat> glColorAt(float val) const;
    float minValue() const;
    float maxValue() const;
signals:
    void limitsChanged(float min, float max);
public slots:
    void setMinValue(float min);
    void setMaxValue(float max);
protected:
    float normalize(float val) const;
    float m_min;
    float m_max;
};

#endif // COLORMAP_H
