#include "magnitudeview.h"

MagnitudeView::MagnitudeView(QSharedPointer<VectorView const> const &parent)
    : ScalarView(),
      m_parent(parent)
{
    connect(parent.data(), SIGNAL(changed()), this, SIGNAL(changed()));
}

float MagnitudeView::valueAt(int x, int y, int z) const
{
    return m_parent->valueAt(x, y, z).length();
}

QPoint MagnitudeView::cellAt(int x, int y) const
{
    return m_parent->cellAt(x, y);
}

QSize MagnitudeView::size() const
{
    return m_parent->size();
}

size_t MagnitudeView::depth() const
{
    return m_parent->depth();
}
