#ifndef SCALEDSCALARVIEW_H
#define SCALEDSCALARVIEW_H

#include <QSharedPointer>
#include "scalarview.h"

class ScaledScalarView : public ScalarView
{
        Q_OBJECT
public:
    ScaledScalarView(QSharedPointer<ScalarView const> const &parent, float scale);
    QSize size() const;
    size_t depth() const;
    float valueAt(int x, int y, int z = 0) const;
    QPoint cellAt(int x, int y) const;

    float scale() const;

signals:
    void scaleChanged(float scale);

public slots:
    void setScale(float scale);

private:
    QSharedPointer<ScalarView const> m_parent;
    float m_scale;
};

#endif // SCALEDSCALARVIEW_H
