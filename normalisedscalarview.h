#ifndef NORMALISEDSCALARVIEW_H
#define NORMALISEDSCALARVIEW_H

#include "scalarview.h"
#include <QSharedPointer>

class NormalisedScalarView : public ScalarView
{
    Q_OBJECT
public:
    NormalisedScalarView(QSharedPointer<ScalarView const> const &parent);
    QSize size() const;
    size_t depth() const;
    float valueAt(int x, int y, int z = 0) const;
    QPoint cellAt(int x, int y) const;
    float scale() const;

signals:
    void scaleChanged(float scale);

protected slots:
    void recalculateScale();

private:
    QSharedPointer<ScalarView const> m_parent;
    float m_scale;
};

#endif // NORMALISEDSCALARVIEW_H
