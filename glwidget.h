#ifndef GLWIDGET_H
#define GLWIDGET_H

#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLVertexArrayObject>
#include <QOpenGLBuffer>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QMatrix4x4>
#include <QVector>
#include <QTimerEvent>
#include <QSharedPointer>
#include "simulation.h"
#include "colormap.h"
#include "scalarview.h"
#include "glcamera.h"

class GLWidget : public QOpenGLWidget, protected QOpenGLFunctions
{
    Q_OBJECT

public:
    GLWidget(QWidget *parent = 0);
    ~GLWidget();

    QSize minimumSizeHint() const Q_DECL_OVERRIDE;
    QSize sizeHint() const Q_DECL_OVERRIDE;

    GLCamera &camera();

    void setData(QSharedPointer<ScalarView const> const &data);

    void setColorMap(QSharedPointer<ColorMap const> const &colorMap);

    bool drawingSmoke() const;
    bool drawingArrows() const;
    bool drawingIsolines() const;

    void setArrowValue(QSharedPointer<ScalarView const> const &data);
    void setArrowDirection(QSharedPointer<VectorView const> const &data);

    float arrowBase() const;
    float arrowScale() const;

signals:
    void mouseDrawing(int x, int y, double dx, double dy);

public slots:
    void setDrawSmoke(bool);
    void setDrawArrows(bool);
    void setDrawIsolines(bool);
    void setIsolines(QVector<float> const &isolines);
    void setArrowBase(float base);
    void setArrowScale(float scale);
    void setRenderDepth(float factor);

    void cleanup();

protected:
    void initializeGL() Q_DECL_OVERRIDE;
    void paintGL() Q_DECL_OVERRIDE;
    void resizeGL(int width, int height) Q_DECL_OVERRIDE;
    void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
    void mouseMoveCameraEvent(QMouseEvent *event);
    void mouseZoomCameraEvent(QMouseEvent *event);
    void mouseInteractWithSimulationEvent(QMouseEvent *event);

private:
    QOpenGLTexture *generateColorMap();

    void initializeSmoke(QSharedPointer<ScalarView const> const &data);
    void updateSmoke(QSharedPointer<ScalarView const> const &data);
    void paintSmoke(QSharedPointer<ScalarView const> const &data);

    void initializeIsolines(QSharedPointer<ScalarView const> const &data);
    void updateIsolines(QSharedPointer<ScalarView const> const &data);
    void paintIsolines(QSharedPointer<ScalarView const> const &data);

    void initializeArrows(QSharedPointer<VectorView const> const &directionData);
    void updateArrows(QSharedPointer<VectorView const> const &directionData, QSharedPointer<ScalarView const> const &scalarData);
    void paintArrows(QSharedPointer<VectorView const> const &directionData, QSharedPointer<ScalarView const> const &scalarData);

    bool m_drawSmoke;
    bool m_drawArrows;
    bool m_drawIsolines;

    QSharedPointer<ScalarView const> m_data;
    QSharedPointer<ColorMap const> m_colorMap;

    QOpenGLVertexArrayObject m_vao;
    QOpenGLBuffer m_smokeVertices;
    QOpenGLBuffer m_smokeColors;

    QOpenGLShaderProgram *m_program;
    QOpenGLShaderProgram *m_isolinesProgram;
    QOpenGLTexture *m_colorMapTexture;
    int m_projMatrixLoc;
    int m_mvMatrixLoc;
    int m_normalMatrixLoc;
    int m_lightPosLoc;
    int m_renderDepthLoc;
    int m_layerIndexLoc;
    int m_layerCountLoc;

    /* Vector visualisation */
    QOpenGLVertexArrayObject m_arrowVao;
    QOpenGLShaderProgram *m_arrowProgram;
    QOpenGLBuffer m_arrows;

    int m_arrowProjMatrixLoc;
    int m_arrowMvMatrixLoc;
    int m_arrowNormalMatrixLoc;
    int m_arrowLightSourceLoc;
    int m_arrowBaseLoc;
    int m_arrowRenderDepthLoc;

    QSharedPointer<ScalarView const> m_arrowValue;
    QSharedPointer<VectorView const> m_arrowDirection;

    float m_arrowBase;
    float m_arrowScale;

    GLCamera m_camera;
    QMatrix4x4 m_proj;
    QVector3D m_lightPos;
    float m_renderDepth;

    QPoint m_lastPos;

    QVector<float> m_isolines;

    bool m_glInitialized;
};

#endif
