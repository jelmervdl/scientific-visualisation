#include "glcamera.h"

GLCamera::GLCamera(QVector3D const &origin)
    : QObject(),
      m_tilt(0),
      m_rotation(0),
      m_zoom(1.0),
      m_origin(origin)
{
    updateCamera();
    updateWorld();
}

void GLCamera::updateCamera()
{
    m_camera.setToIdentity();

    // Move to correct position
    m_camera.translate(-m_origin.x(), -m_origin.y(), -m_zoom * m_origin.z());

    // Tilt at nearest edge
    m_camera.rotate(m_tilt, QVector3D(-1.0, 0, 0));
}

void GLCamera::updateWorld()
{
    m_world.setToIdentity();
    m_world.translate(0.5, 0.5, 0);
    m_world.rotate(m_rotation, 0, 0, 1.0);
    m_world.translate(-0.5, -0.5, 0);
}

QMatrix4x4 const &GLCamera::camera() const
{
    return m_camera;
}

QMatrix4x4 const &GLCamera::world() const
{
    return m_world;
}

QMatrix4x4 GLCamera::moveMatrix() const
{
    return m_camera * m_world;
}

QMatrix3x3 GLCamera::normalMatrix() const
{
    return m_camera.normalMatrix();
}

void GLCamera::rotate(float angle)
{
    m_rotation = angle;
    updateWorld();
    emit changed();
}

void GLCamera::tilt(float angle)
{
    m_tilt = angle;
    updateCamera();
    emit changed();
}

void GLCamera::zoom(float factor)
{
    m_zoom = factor;
    updateCamera();
    emit changed();
}
