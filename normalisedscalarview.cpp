#include "normalisedscalarview.h"
#include <cmath>

NormalisedScalarView::NormalisedScalarView(QSharedPointer<ScalarView const> const &parent)
    : ScalarView(),
      m_parent(parent),
      m_scale(1.0f)
{
    connect(parent.data(), SIGNAL(changed()), this, SLOT(recalculateScale()));

    connect(this, SIGNAL(scaleChanged(float)), this, SIGNAL(changed()));
}

float NormalisedScalarView::scale() const
{
    return m_scale;
}

void NormalisedScalarView::recalculateScale()
{
    float maximum = 0.f;

    for (int i = 0, width = size().width(); i < width; ++i) {
        for (int j = 0, height = size().height(); j < height; ++j) {
            float value = fabs(m_parent->valueAt(i, j, 0));
            if (value > maximum)
                maximum = value;
        }
    }

    m_scale = 1.f / maximum;
    emit scaleChanged(m_scale);
}

float NormalisedScalarView::valueAt(int x, int y, int z) const
{
    return m_scale * m_parent->valueAt(x, y, z);
}

QPoint NormalisedScalarView::cellAt(int x, int y) const
{
    return m_parent->cellAt(x, y);
}

QSize NormalisedScalarView::size() const
{
    return m_parent->size();
}

size_t NormalisedScalarView::depth() const
{
    return m_parent->depth();
}
