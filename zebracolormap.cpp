#include "zebracolormap.h"
#include <cmath>
#include <QDebug>

ZebraColorMap::ZebraColorMap(int patches, QList<QColor> const &colors)
    : m_patches(patches),
      m_colors(colors)
{
    //
}

QColor ZebraColorMap::colorAt(float value) const
{
    float patch_length = 1.0f / m_patches;
    float relative = (value - patch_length * floor(value / patch_length)) / patch_length;
    return m_colors.at(relative * m_colors.size());
}
