#include "colormap.h"

ColorMap::ColorMap()
    : QObject(),
      m_min(0.0f),
      m_max(1.0f)
{
    //
}

QVector<GLfloat> ColorMap::glColorAt(float val) const
{
    QColor color(colorAt(val));
    QVector<GLfloat> parts(3);
    parts[0] = color.redF();
    parts[1] = color.greenF();
    parts[2] = color.blueF();
    return parts;
}

void ColorMap::setMinValue(float minVal)
{
    m_min = minVal;
    emit limitsChanged(m_min, m_max);
}

void ColorMap::setMaxValue(float maxVal)
{
    m_max = maxVal;
    emit limitsChanged(m_min, m_max);
}

float ColorMap::minValue() const
{
    return m_min;
}

float ColorMap::maxValue() const
{
    return m_max;
}

float ColorMap::normalize(float value) const
{
    return qBound(0.0f, (value - m_min) / (m_max - m_min), 1.0f);
}
