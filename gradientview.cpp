#include "gradientview.h"

GradientView::GradientView(QSharedPointer<ScalarView const> const &parent)
    : VectorView(),
      m_parent(parent)
{
    connect(parent.data(), SIGNAL(changed()), this, SIGNAL(changed()));
}

QVector3D GradientView::valueAt(int x, int y, int z) const
{
    // Gradient
    float dx = m_parent->valueAt(x + 1, y, z) - m_parent->valueAt(x - 1, y, z);
    float dy = m_parent->valueAt(x, y + 1, z) - m_parent->valueAt(x, y - 1, z);
    float dz = m_parent->valueAt(x, y, z + 1) - m_parent->valueAt(x, y, z - 1);

    // Divergence
    return QVector3D(dx, dy, dz);
}

QPoint GradientView::cellAt(int x, int y) const
{
    return m_parent->cellAt(x, y);
}

QSize GradientView::size() const
{
    return m_parent->size();
}

size_t GradientView::depth() const
{
    return m_parent->depth();
}
