#ifndef FLOATSLIDER_H
#define FLOATSLIDER_H

#include <QSlider>

class FloatSlider : public QSlider
{
    Q_OBJECT

public:
    FloatSlider(QWidget *parent = Q_NULLPTR);
    FloatSlider(Qt::Orientation orientation, QWidget *parent = Q_NULLPTR);

    float scale() const;
    void setScale(float scale);

    void setScaledValue(float value);
    float scaledValue() const;

signals:
    void valueChanged(float value);

protected slots:
    void updateFloatValue(int);

private:
    float m_scale;
};

#endif // FLOATSLIDER_H
