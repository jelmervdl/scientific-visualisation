#ifndef SCALARVOLUMEVIEW_H
#define SCALARVOLUMEVIEW_H

#include <QSharedPointer>
#include "scalarview.h"

class ScalarVolumeView : public ScalarView
{
    Q_OBJECT
public:
    explicit ScalarVolumeView(QSharedPointer<ScalarView const> const &source, size_t depth, size_t stride, QObject *parent = 0);
    ~ScalarVolumeView();
    QSize size() const;
    size_t depth() const;
    float valueAt(int x, int y, int z = 0) const;
    QPoint cellAt(int x, int y) const;

protected slots:
    void captureFrame();

private:
    QSharedPointer<ScalarView const> m_source;
    float *m_data;
    QSize m_size;
    size_t m_depth;
    size_t m_stride;
    size_t m_frameCount;
    size_t m_frameSize;
    size_t m_offset;
};

#endif // SCALARVOLUMEVIEW_H
