#ifndef SCALARVIEW_H
#define SCALARVIEW_H

#include <QObject>
#include <QPoint>
#include <QSize>

class ScalarView : public QObject
{
    Q_OBJECT
public:
    explicit ScalarView(QObject *parent = 0);
    virtual QSize size() const = 0;
    virtual size_t depth() const = 0;
    virtual float valueAt(int x, int y, int z = 0) const = 0;
    virtual QPoint cellAt(int x, int y) const = 0;

signals:
    void changed();
};

#endif // SCALARVIEW_H
