#ifndef RAINBOWCOLORMAP_H
#define RAINBOWCOLORMAP_H

#include "colormap.h"

class RainbowColorMap : public ColorMap
{
public:
    virtual QColor colorAt(float value) const;
};

#endif // RAINBOWCOLORMAP_H
