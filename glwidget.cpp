#include "glwidget.h"
#include <QMouseEvent>
#include <QOpenGLShaderProgram>
#include <QOpenGLTexture>
#include <QOpenGLPixelTransferOptions>
#include <QCoreApplication>
#include <cmath>
#include "grayscalecolormap.h"

#define CALL_MEMBER_FN(object,ptrToMember)  ((object).*(ptrToMember))

#define PATH_TO_SHADER(name) "/Users/jelmer/Dropbox/Universiteit/Scientific Visualisation/SmokeOSX/shaders/" name
//#define PATH_TO_SHADER(name) ":/shaders/" name


namespace {
    QVector<GLfloat> &normalAt(QVector<GLfloat> &vec, ScalarView const &data, int i, int j, int z) {
        return vec << (data.valueAt(i - 1, j, z) - data.valueAt(i + 1, j, z)) / 2
                   << (data.valueAt(i, j - 1, z) - data.valueAt(i, j + 1, z)) / 2;
    }
}

GLWidget::GLWidget(QWidget *parent)
    : QOpenGLWidget(parent),
      m_drawSmoke(true),
      m_drawArrows(true),
      m_drawIsolines(true),
      m_data(0),
      m_colorMap(new GrayscaleColorMap()),
      m_program(0),
      m_colorMapTexture(0),
      m_arrowProgram(0),
      m_arrowValue(0),
      m_arrowDirection(0),
      m_arrowBase(0.1),
      m_arrowScale(0.1),
      m_camera(QVector3D(0.5, 0.5, 1.2)),
      m_lightPos(QVector3D(3, -3, 3)),
      m_renderDepth(0.f),
      m_glInitialized(false)
{
    setAttribute(Qt::WA_TranslucentBackground);
    connect(&m_camera, SIGNAL(changed()), this, SLOT(update()));
}

GLWidget::~GLWidget()
{
    cleanup();
}

QSize GLWidget::minimumSizeHint() const
{
    return QSize(50, 50);
}

QSize GLWidget::sizeHint() const
{
    return QSize(600, 600);
}

void GLWidget::cleanup()
{
    makeCurrent();
    m_smokeVertices.destroy();
    m_smokeColors.destroy();
    delete m_program;
    m_program = 0;
    doneCurrent();
}

GLCamera &GLWidget::camera()
{
    return m_camera;
}

void GLWidget::setData(QSharedPointer<ScalarView const> const &data)
{
    if (m_data != 0)
        disconnect(m_data.data(), SIGNAL(changed()), this, SLOT(update()));

    if (m_glInitialized && (m_data == 0 || m_data->size() != data->size())) {
        initializeSmoke(data);
        initializeIsolines(data);
    }

    m_data = data;
    connect(m_data.data(), SIGNAL(changed()), this, SLOT(update()));
}

QOpenGLTexture *GLWidget::generateColorMap()
{
    int n = 512;

    QImage image(n, 1, QImage::Format_RGB32);
    for (int i = 0; i < n; ++i) {
        image.setPixel(i, 0, m_colorMap->colorAt((float) i / (n - 1)).rgb());
    }

    QOpenGLTexture *colorMapTexture = new QOpenGLTexture(image);
    colorMapTexture->setWrapMode(QOpenGLTexture::ClampToEdge);

    return colorMapTexture;
}

void GLWidget::initializeSmoke(QSharedPointer<ScalarView const> const &data)
{
    QSize size(data->size());

    QVector<GLfloat> smoke;
    smoke.reserve(size.width() * size.height() * 6 * 3);

    float  wn = 1.0f / (size.width() + 1);  // Grid cell width
    float  hn = 1.0f / (size.height() + 1);  // Grid cell heigh

    // Create a huge constant mesh of triangles which will be the base
    // for our smoke and isolines visualisations.
    //
    // A --- B
    // |   / |
    // | /   |
    // C --- D
    //
    for (int j = 0; j < size.height() - 1; j++)
    {
        for (int i = 0; i < size.width() - 1; i++)
        {
            GLfloat px0  = wn + i * wn;
            GLfloat py0  = hn + j * hn;
            //size_t idx0 = (j * m_resolution) + i;

            GLfloat px1  = wn + i * wn;
            GLfloat py1  = hn + (j + 1) * hn;
            //size_t idx1 = ((j + 1) * m_resolution) + i;

            GLfloat px2  = wn + (i + 1) * wn;
            GLfloat py2  = hn + (j + 1) * hn;
            //size_t idx2 = ((j + 1) * m_resolution) + (i + 1);

            GLfloat px3  = wn + (i + 1) * wn;
            GLfloat py3  = hn + j * hn;
            //size_t idx3 = (j * m_resolution) + (i + 1);

            // C: i, j
            smoke.append(px0);
            smoke.append(py0);
            smoke.append(0);

            // A: i, j + 1
            smoke.append(px1);
            smoke.append(py1);
            smoke.append(0);

            // B: i + 1, j + 1
            smoke.append(px2);
            smoke.append(py2);
            smoke.append(0);

            // C: i, j
            smoke.append(px0);
            smoke.append(py0);
            smoke.append(0);

            // B: i + 1, j + 1
            smoke.append(px2);
            smoke.append(py2);
            smoke.append(0);

            // D: i + 1, j
            smoke.append(px3);
            smoke.append(py3);
            smoke.append(0);
        }
    }

    // In case we are re-initialising, delete current state first
    if (m_program != 0)
        delete m_program;

    if (m_smokeVertices.isCreated())
        m_smokeVertices.destroy();

    if (m_smokeColors.isCreated())
        m_smokeColors.destroy();

    m_program = new QOpenGLShaderProgram;
    m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, PATH_TO_SHADER("smoke.vert"));
    m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, PATH_TO_SHADER("smoke.frag"));
    m_program->bindAttributeLocation("vertex", 0);
    m_program->bindAttributeLocation("value", 1);
    m_program->bindAttributeLocation("difference", 2);
    if (!m_program->link()) {
        qWarning() << "Could not link smoke shader program:" << m_program->log();
        exit(EXIT_FAILURE);
    }

    m_program->bind();
    m_projMatrixLoc = m_program->uniformLocation("projMatrix");
    m_mvMatrixLoc = m_program->uniformLocation("mvMatrix");
    m_normalMatrixLoc = m_program->uniformLocation("normalMatrix");
    m_lightPosLoc = m_program->uniformLocation("lightPos");
    m_renderDepthLoc = m_program->uniformLocation("renderDepth");
    m_layerIndexLoc = m_program->uniformLocation("layerIndex");
    m_layerCountLoc = m_program->uniformLocation("layerCount");

    // Setup our vertex buffer object.
    m_smokeVertices.create();
    m_smokeVertices.setUsagePattern(QOpenGLBuffer::StaticDraw);
    m_smokeVertices.bind();
    m_smokeVertices.allocate(smoke.constData(), smoke.count() * sizeof(GLfloat));
    m_smokeVertices.release();

    m_smokeColors.create();
    m_smokeColors.setUsagePattern(QOpenGLBuffer::DynamicDraw);
    m_smokeColors.bind();
    m_smokeColors.allocate(size.width() * size.height() * data->depth() * 6 * 3 * sizeof(GLfloat));
    m_smokeColors.release();

    // Create a vertex array object. In OpenGL ES 2.0 and OpenGL 2.x
    // implementations this is optional and support may not be present
    // at all. Nonetheless the below code works in all cases and makes
    // sure there is a VAO when one is needed.
    QOpenGLVertexArrayObject::Binder vaoBinder(&m_vao);

    // Store the vertex attribute bindings for the program.
    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
    f->glEnableVertexAttribArray(0);
    f->glEnableVertexAttribArray(1);
    f->glEnableVertexAttribArray(2);

    m_smokeVertices.bind();
    f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    m_smokeVertices.release();

    m_smokeColors.bind();
    f->glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
    f->glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void *) (1 * sizeof(GLfloat)));
    m_smokeColors.release();

    m_program->release();
}

void GLWidget::updateSmoke(QSharedPointer<ScalarView const> const &data)
{
    QSize size(data->size());

    QVector<GLfloat> colors;
    colors.reserve(size.width() * size.height() * 6 * (1 + 2));

    // A --- B
    // |   / |
    // | /   |
    // C --- D
    for (size_t z = 0; z < data->depth(); ++z) {
        for (int j = 0; j < size.height() - 1; ++j) {
            for (int i = 0; i < size.width() - 1; ++i) {
                // C
                colors << data->valueAt(i, j, z);
                normalAt(colors, *data, i, j, z);

                // A
                colors << m_data->valueAt(i, j + 1, z);
                normalAt(colors, *data, i, j + 1, z);

                // B
                colors << m_data->valueAt(i + 1, j + 1, z);
                normalAt(colors, *data, i + 1, j + 1, z);

                // C
                colors << m_data->valueAt(i, j, z);
                normalAt(colors, *data, i, j, z);

                // B
                colors << m_data->valueAt(i + 1, j + 1, z);
                normalAt(colors, *data, i + 1, j + 1, z);

                 // D
                colors << m_data->valueAt(i + 1, j, z);
                normalAt(colors, *data, i + 1, j, z);
            }
        }
    }

    m_smokeColors.bind();
    m_smokeColors.write(0, colors.constData(), colors.count() * sizeof(GLfloat));
    m_smokeColors.release();
}

void GLWidget::paintSmoke(QSharedPointer<ScalarView const> const &data)
{
    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    QOpenGLVertexArrayObject::Binder vaoBinder(&m_vao);
    m_program->bind();
    m_program->setUniformValue(m_projMatrixLoc, m_proj);
    m_program->setUniformValue(m_mvMatrixLoc, m_camera.moveMatrix());
    m_program->setUniformValue(m_normalMatrixLoc, m_camera.normalMatrix());

    m_program->setUniformValue("colormap", 0);
    m_program->setUniformValue(m_lightPosLoc, m_lightPos);
    m_program->setUniformValue(m_renderDepthLoc, m_renderDepth);
    m_program->setUniformValue(m_layerCountLoc, (GLint) data->depth());

    size_t layerSize = data->size().width() * data->size().height();

    glGetError(); // empty error buff

    m_smokeColors.bind();

    // Paint in back-to-front order for alpha blending
    for (int z = data->depth() - 1; z >= 0; --z) {
        m_program->setUniformValue(m_layerIndexLoc, (GLint) z);

        // Move the offset in the colors/values array a bit
        glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void *) (6 * 3 * layerSize * z * sizeof(GLfloat)));
        glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void *) ((1 + 6 * 3 * layerSize * z) * sizeof(GLfloat)));

        glDrawArrays(GL_TRIANGLES, 0, layerSize * 6);
    }
    m_smokeColors.release();

    m_program->release();

    glDisable(GL_BLEND);
}

void GLWidget::initializeArrows(QSharedPointer<VectorView const> const &data)
{
    QOpenGLVertexArrayObject::Binder vaoBinder(&m_arrowVao);

    if (m_arrowProgram != 0)
        delete m_arrowProgram;

    if (m_arrows.isCreated())
        m_arrows.destroy();

    m_arrowProgram = new QOpenGLShaderProgram;
    m_arrowProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, PATH_TO_SHADER("arrows.vert"));
    m_arrowProgram->addShaderFromSourceFile(QOpenGLShader::Geometry, PATH_TO_SHADER("arrows.geom"));
    m_arrowProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, PATH_TO_SHADER("arrows.frag"));
    m_arrowProgram->bindAttributeLocation("vertex", 0);
    m_arrowProgram->bindAttributeLocation("value", 1);
    if (!m_arrowProgram->link()) {
        qWarning() << "Could not link arrow shader program:" << m_arrowProgram->log();
        exit(EXIT_FAILURE);
    }

    m_arrowProgram->bind();
    m_arrowProjMatrixLoc = m_arrowProgram->uniformLocation("projMatrix");
    m_arrowMvMatrixLoc = m_arrowProgram->uniformLocation("mvMatrix");
    m_arrowLightSourceLoc = m_arrowProgram->uniformLocation("lightPos");
    m_arrowNormalMatrixLoc = m_arrowProgram->uniformLocation("normalMatrix");
    m_arrowBaseLoc = m_arrowProgram->uniformLocation("arrowBase");
    m_arrowRenderDepthLoc = m_arrowProgram->uniformLocation("renderDepth");

    if (m_arrows.isCreated())
        m_arrows.destroy();

    m_arrows.create();
    m_arrows.setUsagePattern(QOpenGLBuffer::DynamicDraw);
    m_arrows.bind();
    m_arrows.allocate(2 * 3 * data->size().width() * data->size().height() * sizeof(GLfloat)); // a line (two points) with x, y and value floats for each cell.
    m_arrows.release();

    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
    f->glEnableVertexAttribArray(0);
    f->glEnableVertexAttribArray(1);

    m_arrows.bind();
    f->glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
    f->glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void*) (2 * sizeof(GLfloat)));
    m_arrows.release();

    m_arrowProgram->release();
}

void GLWidget::updateArrows(QSharedPointer<VectorView const> const &directionData, QSharedPointer<ScalarView const> const &scalarData)
{
    QSize size(directionData->size());

    float  wn = 1.0f / (size.width() + 1);  // Grid cell width
    float  hn = 1.0f / (size.height() + 1);  // Grid cell heigh

    QVector<GLfloat> points;
    points.reserve(2 * 3 * size.width() * size.height()); // lines; two points of x, y and value.

    for (int i = 0; i < size.width(); ++i) {
        for (int j = 0; j < size.height(); ++j) {
            float val = scalarData->valueAt(i, j);
            QVector3D dir = directionData->valueAt(i, j);

            // Origin
            points.append(wn + i * wn);
            points.append(hn + j * hn);

            // Origin value
            points.append(val);

            points.append((wn + i * wn) + m_arrowScale * dir.x());
            points.append((hn + j * hn) + m_arrowScale * dir.y());

            // Target value (has to be the same due to how the shaders are implemented…)
            points.append(val);
        }
    }

    m_arrows.bind();
    m_arrows.write(0, points.constData(), points.count() * sizeof(GLfloat));
    m_arrows.release();
}

void GLWidget::paintArrows(QSharedPointer<VectorView const> const &directionData, QSharedPointer<ScalarView const> const &scalarData)
{
    Q_UNUSED(scalarData);

    QOpenGLVertexArrayObject::Binder vaoBinder(&m_arrowVao);
    m_arrowProgram->bind();
    m_arrowProgram->setUniformValue(m_arrowProjMatrixLoc, m_proj);
    m_arrowProgram->setUniformValue(m_arrowMvMatrixLoc, m_camera.moveMatrix());
    m_arrowProgram->setUniformValue(m_arrowNormalMatrixLoc, m_camera.normalMatrix());
    m_arrowProgram->setUniformValue(m_arrowLightSourceLoc, m_lightPos);
    m_arrowProgram->setUniformValue(m_arrowBaseLoc, m_arrowBase);
    m_arrowProgram->setUniformValue(m_arrowRenderDepthLoc, m_renderDepth + 0.1f);

    // We're sending lines to the vbo, but the geometry shader outputs triangles
    glDrawArrays(GL_LINES, 0, directionData->size().width() * directionData->size().height() * 2);

    m_arrowProgram->release();
}

void GLWidget::initializeIsolines(QSharedPointer<ScalarView const> const &data)
{
    Q_UNUSED(data);

    if (m_isolinesProgram != 0)
        delete m_isolinesProgram;

    m_isolinesProgram = new QOpenGLShaderProgram;
    m_isolinesProgram->addShaderFromSourceFile(QOpenGLShader::Vertex, PATH_TO_SHADER("isolines.vert"));
    m_isolinesProgram->addShaderFromSourceFile(QOpenGLShader::Geometry, PATH_TO_SHADER("isolines.geom"));
    m_isolinesProgram->addShaderFromSourceFile(QOpenGLShader::Fragment, PATH_TO_SHADER("isolines.frag"));
    m_isolinesProgram->bindAttributeLocation("vertex", 0);
    m_isolinesProgram->bindAttributeLocation("value", 1);
    if (!m_isolinesProgram->link()) {
        qWarning() << "Could not link isolines shader program:" << m_arrowProgram->log();
        exit(EXIT_FAILURE);
    }

    // Create a vertex array object. In OpenGL ES 2.0 and OpenGL 2.x
    // implementations this is optional and support may not be present
    // at all. Nonetheless the below code works in all cases and makes
    // sure there is a VAO when one is needed.
    QOpenGLVertexArrayObject::Binder vaoBinder(&m_vao);

    m_program->bind();
    m_projMatrixLoc = m_isolinesProgram->uniformLocation("projMatrix");
    m_mvMatrixLoc = m_isolinesProgram->uniformLocation("mvMatrix");
    m_normalMatrixLoc = m_isolinesProgram->uniformLocation("normalMatrix");

    // Store the vertex attribute bindings for the program.
    QOpenGLFunctions *f = QOpenGLContext::currentContext()->functions();
    f->glEnableVertexAttribArray(0);
    f->glEnableVertexAttribArray(1);

    m_smokeVertices.bind();
    f->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);
    m_smokeVertices.release();

    m_smokeColors.bind();
    f->glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), 0);
    m_smokeColors.release();

    m_isolinesProgram->release();
}

void GLWidget::updateIsolines(QSharedPointer<ScalarView const> const &data)
{
    Q_UNUSED(data);
    // Hahaha do nothing. Just make sure the smoke gets updated.
}

void GLWidget::paintIsolines(QSharedPointer<ScalarView const> const &data)
{
    QOpenGLVertexArrayObject::Binder vaoBinder(&m_vao);
    m_isolinesProgram->bind();
    m_isolinesProgram->setUniformValue("projMatrix", m_proj);
    m_isolinesProgram->setUniformValue("mvMatrix", m_camera.moveMatrix());
    m_isolinesProgram->setUniformValue("normalMatrix", m_camera.normalMatrix());
    m_isolinesProgram->setUniformValue("layerCount", (GLint) data->depth());

    m_isolinesProgram->bind();
    m_isolinesProgram->setUniformValue("colormap", 0);

    m_smokeColors.bind();

    for (int z = data->depth() - 1; z >= 0; --z) {
        // Move pointer to the values for the right layer
        m_program->setUniformValue("layerIndex", (GLint) z);
        glVertexAttribPointer(1, 1, GL_FLOAT, GL_FALSE, 3 * sizeof(GLfloat), (void *) (6 * 3 * data->size().width() * data->size().height() * z * sizeof(GLfloat)));

        // Draw each isoline separately
        foreach (float isoline, m_isolines) {
            m_isolinesProgram->setUniformValue("t", isoline);
            glDrawArrays(GL_TRIANGLES, 0, data->size().width() * data->size().height() * 6);
        }
    }
    m_smokeColors.release();

    m_isolinesProgram->release();
}

void GLWidget::initializeGL()
{
    // In this example the widget's corresponding top-level window can change
    // several times during the widget's lifetime. Whenever this happens, the
    // QOpenGLWidget's associated context is destroyed and a new one is created.
    // Therefore we have to be prepared to clean up the resources on the
    // aboutToBeDestroyed() signal, instead of the destructor. The emission of
    // the signal will be followed by an invocation of initializeGL() where we
    // can recreate all resources.
    connect(context(), &QOpenGLContext::aboutToBeDestroyed, this, &GLWidget::cleanup);

    initializeOpenGLFunctions();
    glClearColor(0, 0, 0, 0);

    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    glEnable(GL_TEXTURE);
    glEnable(GL_DEPTH_TEST);

    m_vao.create();
    m_arrowVao.create();

    m_colorMapTexture = generateColorMap();

    // If data is already set, initialize the buffers

    if (!m_data.isNull())
        initializeSmoke(m_data);

    if (!m_arrowDirection.isNull())
        initializeArrows(m_arrowDirection);

    if (!m_data.isNull())
        initializeIsolines(m_data);

    m_glInitialized = true;
}

void GLWidget::paintGL()
{
    if (m_data == 0)
        return;

    m_colorMapTexture->bind();

    // Upload data to GPU

    if (m_drawSmoke || (m_drawIsolines && !m_isolines.empty()))
        updateSmoke(m_data);

    if (m_drawArrows && m_arrowDirection != 0 && m_arrowValue != 0)
        updateArrows(m_arrowDirection, m_arrowValue);

    if (m_drawIsolines && !m_isolines.empty())
        updateIsolines(m_data);

    // Let's draw!

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

    if (m_drawSmoke)
        paintSmoke(m_data);

    if (m_drawArrows && m_arrowDirection != 0 && m_arrowValue != 0)
        paintArrows(m_arrowDirection, m_arrowValue);

    if (m_drawIsolines && !m_isolines.empty())
        paintIsolines(m_data);
}

void GLWidget::resizeGL(int w, int h)
{
    m_proj.setToIdentity();
    m_proj.perspective(45.0f, GLfloat(w) / h, 0.01f, 100.0f);
}

void GLWidget::mousePressEvent(QMouseEvent *event)
{
    m_lastPos = event->pos();
}

void GLWidget::mouseMoveEvent(QMouseEvent *event)
{
    if (event->modifiers() & Qt::AltModifier)
        mouseMoveCameraEvent(event);
    else if (event->modifiers() & Qt::ShiftModifier)
        mouseZoomCameraEvent(event);
    else
        mouseInteractWithSimulationEvent(event);
}

void GLWidget::mouseInteractWithSimulationEvent(QMouseEvent *event)
{
    // Compute the position in the grid of the mouse
    int size = qMin(width(), height());

    double ex = event->x() - (width() - size) / 2.0;
    double ey = event->y() - (height() - size) / 2.0;

    int xi = qRound((double) m_data->size().width() * ex / size);
    int yi = qRound((double) m_data->size().height() * ey / size);

    // Only if we're moving inside the area covered by the simulation, handle events
    if (xi < 0 || yi < 0 || xi >= (int) m_data->size().width() || yi >= (int) m_data->size().height())
        return;

    // Add force at the cursor location int the grid
    double dx = event->x() - m_lastPos.x();
    double dy = event->y() - m_lastPos.y();
    double len = sqrt(dx * dx + dy * dy);

    if (len != 0.0) {
        dx *= 0.1 / len;
        dy *= 0.1 /len;
    }

    QPoint view_point(m_data->cellAt(xi, m_data->size().height() - yi));
    emit mouseDrawing(view_point.x(), view_point.y(), dx, -dy);

    m_lastPos = event->pos();
}

bool GLWidget::drawingArrows() const
{
    return m_drawArrows;
}

void GLWidget::setDrawArrows(bool enable)
{
    m_drawArrows = enable;
}

bool GLWidget::drawingSmoke() const
{
    return m_drawSmoke;
}

void GLWidget::setDrawSmoke(bool enable)
{
    m_drawSmoke = enable;
}

void GLWidget::setColorMap(QSharedPointer<ColorMap const> const &colorMap)
{
    m_colorMap = colorMap;

    // Only regenerate the color map texture when it already exists,
    // otherwise we haven't initialized OpenGL far enough yet to
    // generate it anyway, and it will be generated when it is time.
    if (m_colorMapTexture != 0) {
        QOpenGLTexture *colorMapTexture = generateColorMap();
        delete m_colorMapTexture;
        m_colorMapTexture = colorMapTexture;
    }

    // Trigger redraw
    update();
}

bool GLWidget::drawingIsolines() const
{
    return m_drawIsolines;
}

void GLWidget::setDrawIsolines(bool enable)
{
    m_drawIsolines = enable;
    update();
}

void GLWidget::setIsolines(const QVector<float> &isolines)
{
    m_isolines = isolines;
    update();
}

void GLWidget::setArrowValue(QSharedPointer<ScalarView const> const &data)
{
    Q_ASSERT(m_arrowDirection != 0);
    Q_ASSERT(data->size() == m_arrowDirection->size());

    if (m_arrowValue != 0)
        disconnect(m_arrowValue.data(), SIGNAL(changed()), this, SLOT(update()));

    m_arrowValue = data;
    connect(m_arrowValue.data(), SIGNAL(changed()), this, SLOT(update()));
}

void GLWidget::setArrowDirection(QSharedPointer<VectorView const> const &data)
{
    if (m_arrowDirection != 0)
        disconnect(m_arrowDirection.data(), SIGNAL(changed()), this, SLOT(update()));

    // If we already had a dataset but this one is of a different dimension, reinit GL resources
    if (m_glInitialized && (m_arrowDirection == 0 || m_arrowDirection->size() != data->size()))
        initializeArrows(data);

    m_arrowDirection = data;
    connect(m_arrowDirection.data(), SIGNAL(changed()), this, SLOT(update()));
}

float GLWidget::arrowBase() const
{
    return m_arrowBase;
}

void GLWidget::setArrowBase(float base)
{
    m_arrowBase = base;
    update();
}

float GLWidget::arrowScale() const
{
    return m_arrowScale;
}

void GLWidget::setArrowScale(float scale)
{
    m_arrowScale = scale;
    update();
}

void GLWidget::mouseMoveCameraEvent(QMouseEvent *event)
{
    float x = qBound(0.f, (float) event->x() / width(), 1.f);
    float y = qBound(0.f, (float) event->y() / height(), 1.f);

    m_camera.rotate(180.f * (x - 0.5f));
    m_camera.tilt(90.f * y);
}

void GLWidget::mouseZoomCameraEvent(QMouseEvent *event)
{
    float y = qBound(0.f, (float) event->y() / height(), 1.f);
    m_camera.zoom(3.f * y);
}

void GLWidget::setRenderDepth(float factor)
{
    m_renderDepth = qBound(0.0f, factor, 1.0f);
}
