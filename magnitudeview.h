#ifndef MAGNITUDEVIEW_H
#define MAGNITUDEVIEW_H

#include "scalarview.h"
#include "vectorview.h"
#include <QObject>
#include <QSharedPointer>

class MagnitudeView : public ScalarView        
{
    Q_OBJECT
public:
    MagnitudeView(QSharedPointer<VectorView const> const &parent);
    QSize size() const;
    size_t depth() const;
    float valueAt(int x, int y, int z = 0) const;
    QPoint cellAt(int x, int y) const;
private:
    QSharedPointer<VectorView const> m_parent;
};


#endif // MAGNITUDEVIEW_H
