#ifndef GLCAMERA_H
#define GLCAMERA_H

#include <QObject>
#include <QVector3d>
#include <QMatrix4x4>

class GLCamera: public QObject
{
    Q_OBJECT

public:
    GLCamera(QVector3D const &origin);
    QMatrix4x4 const &camera() const;
    QMatrix4x4 const &world() const;
    QMatrix4x4 moveMatrix() const;
    QMatrix3x3 normalMatrix() const;

public slots:
    void rotate(float angle);
    void tilt(float angle);
    void zoom(float factor);

signals:
    void changed();

protected:
    void updateCamera();
    void updateWorld();

private:
    float m_tilt;
    float m_rotation;
    float m_zoom;
    QVector3D m_origin;
    QMatrix4x4 m_camera;
    QMatrix4x4 m_world;
};

#endif // GLCAMERA_H
